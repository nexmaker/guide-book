# Summary

## [Lab manage](doc/0manage/labmanage.md)
  * [Fab Lab ](doc/Fab/FAB.md)
  * [NexMaker Academy](doc/0manage/nexmaker-academy.md)
  * [Access and safety](doc/0manage/access_safety.md)
  * [Material](doc/0manage/material.md)
  * [Tool](doc/0manage/tool.md)
   
 

## Tutorials
* [1.Project manage](https://git-scm.com/)
  * [Assessment](doc/1projectmanage/Assessment1.md)
  * [Github page&docsify](doc/1projectmanage/github&docsify.md)
  * [Web build method](doc/1projectmanage/webmethod.md)
    * [Gitlab-html](https://pages.gitlab.io/plain-html/):gitlab to  build static website
    * [Gitlab & gitgbook](doc/1projectmanage/gitbook.md)
    * [VuePress](https://vuepress.vuejs.org/): similar with gitbook
    * [Wordpress web servier](doc/1projectmanage/wordpresswebservier.md) use wordpress to build website
    * [Tech Stack - HTML+CSS+Github](doc/1projectmanage/htmlcssgithub.md)
    * [Wix](doc/1projectmanage/wix.md)
    * [ Tencent Cloud & Next.js](https://design-engineering.littleor.cn/docs/projects/website)
    * [Github & Hexo](https://mengcye.github.io/2023/10/16/WebPageBuild/)
    * [Figma with github](https://nexmaker-fab.github.io/2023zjudem-Tomorrow-s-Wonder-Team/PM.html)

  * [Tool](doc/1projectmanage/Tool.md)
    * [Git](doc/1projectmanage/git.md)
    * [Gitlab](https://gitlab.com/)
    * [Github](https://github.com/)
    * [Markdown](doc/1projectmanage/markdown.md)
    * [Image uploader service](doc/1projectmanage/imageuploadservice.md)
    * [HTML](https://www.w3school.com.cn/html/index.asp)
    * [CSS](https://www.w3school.com.cn/css/index.asp)
    * [AIGC](doc/1projectmanage/aigc.md)

*  [2. CAD design](doc/2cad/cad.md)
  * [Assessment](doc/2cad/Assessment.md)
  * [Fusion 360 prepare](doc/2cad/Fusion360prepare.md)
  * [Fusion 360-Guidebook](doc/2cad/3D_Design_Fusion360.md)
  * [Solidworks-guidebook](doc/2cad/Solidworks.md)
  * [Parameter design](doc/2cad/parameterdesign.md) 
  * [Sheet metal](doc/2cad/fusion360-sheetmetal.md)
  * [AI design](doc/2cad/newdesign.md)
  * [practice](doc/2cad/practice.md)
 
* [3. 3D printer](doc/3_3dprinter/assessment.md)
  * [Assessnment](doc/3_3dprinter/assessment.md)
  * [FDM practice](doc/3_3dprinter/3dprintingdesign.md)
  * [3D printer background](doc/3_3dprinter/1.3Dprintingbackground.md)
  * [FDM background](doc/3_3dprinter/2.FDM3Dprintingbackground.md)
  * [FDM design guide](doc/3_3dprinter/3.FDM-designguide.md)
  * [FDM machine operation](doc/3_3dprinter/4.FDM-machineoperation.md)
  * [SLA background](doc/3_3dprinter/5.SLAbackground.md)
  * [SLA design guide](doc/3_3dprinter/6.sladesignguide.md)
  * [HP-MJF](doc/3_3dprinter/7.hpmjf.md)
  * [Post process](doc/3_3dprinter/8.postprocess.md)
  * [Slice software](doc/3_3dprinter/9.3Dslicesoftware.md)

* [4. Electric design ](doc/4electric_design/basicknowledge.md)
  * [Assessment](doc/4electric_design/Assessment.md)
  * [Electric component](doc/4electric_design/electricparameter_component.md)
  * [EAGLE](https://knowledge.autodesk.com/support/eagle/troubleshooting/caas/sfdcarticles/sfdcarticles/Autodesk-EAGLE-now-included-with-Fusion-360.html)
  * [Fusion 360 electronics](https://www.autodesk.com/products/fusion-360/electronics-engineer)
  * [Cadence](https://www.cadence.com/en_US/home.html)
  * [Altium Designer](https://www.altium.com/altium-designer/)
  * [eebasic](doc/4electric_design/ee.md)
* [5. Arduino application](https://www.arduino.cc/)
  * [Assessment](doc/5arduino/assessment.md)
  * [Open source](doc/5arduino/open_source.md)
  * [Arduino Basic](doc/5arduino/arduino_basic.md)
  * [Arduino Code](doc/5arduino/arduino_code.md)
  * [Arduino Input](doc/5arduino/Arduino_Input.md)
  * [Arduino Output](doc/5arduino/Arduino_output.md)
  * [Arduino IOT](doc/9IOT/NodeMCUESP8266_ALiYun.md)
  * [Library](https://www.arduino.cc/en/Tutorial/LibraryExamples)
  * [Processing](doc/5arduino/processing.md)
  * [Mechancial arm](doc/5arduino/robot-mycobot.md)
      
* [6. Laser cutting](doc/6laser_cutter/basic.md)
  * [Assessment](doc/6laser_cutter/Assessment.md)
  * [Safety](doc/6laser_cutter/Safety.md)
  * [AutoCAD](doc/6laser_cutter/AutoCAD.md)
  * [Design guide](doc/6laser_cutter/Design_guide.md)
  * [Machine practice](doc/6laser_cutter/Machine_practice.md)
  
* [7. PCB manufacture](https://www.nexpcb.com/blog/smt-pcb-puzzle)
  * [Assessment](doc/8CNC_manufacture/Assessment.md)
  * [PCB](doc/4electric_design/electricparameter_component.md)
  * [SMT](doc/7PCB_manufacture/SMT.md)
  * [Manual Soldering](doc/7PCB_manufacture/manual.md)
		
* [8. CNC manufacture](https://astromachineworks.com/what-is-cnc-machining/)
  * [Assessment](doc/8CNC_manufacture/Assessment.md)
  * [Types of CNC Machines ](doc/8CNC_manufacture/cnctype.md)
  * [Desktop Tool](doc/8CNC_manufacture/tool.md)
  * [Desktop 3xis CNC Program](doc/8CNC_manufacture/cncprogram.md)
  * [Desktop 3xis CNC Manufacture](doc/8CNC_manufacture/cncmanufacture.md)

* [9.Mold](doc/9MOLD/mold.md)
  * [Assessment](doc/9MOLD/assessment.md)
  * [Material](doc/9MOLD/material.md)
  * [Tool](doc/9MOLD/tool.md)
  * [Method](doc/9MOLD/method.md)

*  [10. IOT and Interaction](https://en.wikipedia.org/wiki/Internet_of_Things)
  * [Assessment](doc/10IOT/Assessment.md)
  * [IOT basic introduce](doc/10IOT/IOT_basic.md)
  * [NodeMCU-Aliyun Cloud](doc/10IOT/NodeMCUESP8266_ALiYun.md)
  
* [11. Interface Application Programming](http://fabacademy.org/2018/docs/FabAcademy-Tutorials/week16_interface_and_application_programming/processing_resources.html)
  * [Assessment](doc/11Interface-application-programming/Assessment.md)
  * [Processing](doc/11Interface-application-programming/processing.md)
  * [Processing with Arduino](doc/11Interface-application-programming/processingwitharduino.md)
  * [example](doc/11Interface-application-programming/piying.md)
  * [Other tool](doc/11Interface-application-programming/tool.md)
 
* [12.Material and tool](doc/12material&tool/standardcomponentpart.md)
  * [Assessment](doc/12material&tool/assessment.md)
  * [Material](doc/12material&tool/material.md)
  * [ME standard component and tools](http://www.misumi.com.cn/)
  * EE component:[Digi-Key](https://www.digikey.cn/),[Mouser](https://www.mouser.cn/)
  
* [13.Business plan basic](doc/13BP/README.md)
  * [Assessment](doc/13BP/assessment.md)

* [14.AI vehicle](doc/14AI&vehicle/basic.md)
  * [Assessment](doc/14AI&vehicle/assessment.md)
  * [AI platform](doc/14AI&vehicle/aiplatform.md)

## FABS

* [NexMaker academy-1](class/fab-01.md)
  * [Jiahao Liu](https://nex-fab.gitlab.io/fab-01/jiahaoliu/)
  * [Qihao Bao](https://nex-fab.gitlab.io/fab-01/qihaobao/)
  * [Jocelin](https://nex-fab.gitlab.io/fab-01/jocelin/)
  * [Lei chen](https://nex-fab.gitlab.io/fab-01/chenlei/)
  * [Yijun Wang](https://nex-fab.gitlab.io/fab-01/yijunwang/)
  * [luis Li](https://nex-fab.gitlab.io/fab-01/luis/)
  * [Liang Guo](https://nex-fab.gitlab.io/fab-01/gl/)
  * [Jacen Wang](https://nex-fab.gitlab.io/fab-01/jacen/)
  * [Mars Ma](https://nex-fab.gitlab.io/fab-01/marsma/)
  * [Bo Li](https://nex-fab.gitlab.io/fab-01/bo-li/)
  * [Bob Wu](https://nex-fab.gitlab.io/fab-01/bobstudent/)

* [NexMaker academy-2](class/fab-02.md)
  * [June](https://nex-fab.gitlab.io/fab-02/june/)
  * [Otto](https://nex-fab.gitlab.io/fab-02/otto)
  * [Ciel](https://nex-fab.gitlab.io/fab-02/ciel)
  * [Jenny](https://nex-fab.gitlab.io/fab-02/jenny)
  * [Orkun](https://nex-fab.gitlab.io/fab-02/orkun )
  * [Candy]( https://nex-fab.gitlab.io/fab-02/candy/)

* [Design Engineering2020](class/fab-2020-zju-de.md)
  * [924](http://team-924.gitlab.io/zjucst924/)
    * [924 Final project](class/zju2020/924.md)
  * [Duck Dog](https://1909097669.wixsite.com/duck-dog)
  * [Terminus](http://www.0xing.cn/home/blog/)
  * [D&L](class/zju2020/dlight.md)
  * [Happyplanet](https://happyplanet196.wixsite.com/happyplanet)
    * [happly planet Final project](class/zju2020/happlyplanet.md)
  * [2.4GHZ](http://49.235.203.145)
  * [Coffe Bit](https://manatee257.github.io)
    * [Coffe Bit Final project](class/zju2020/cb.md)
  * [33Designer](https://yyniao.github.io)
    * [Final project](class/zju2020/33design.md)
    * [Video](https://gitlab.com/nexmaker/zjunb-fab-01/video/-/raw/main/33designer.mp4)
  * [YYclub](http://yyclub.designist.cn)
    * [YYclub Final project](class/zju2020/yyclub.md)
  * [Morning](https://1348328828.wixsite.com/mysite-2)
    * [Final project &vidoe](class/zju2020/morning.md)
  * [Natural](http://natural.designist.cn) 
    * [Natural Final project](class/zju2020/natural.md)
    * [video test](class/zju2020/video.md)
  
* [Design Engineering2021 ](class/fab-2021-zju-de.md)
  * [CNFD](https://374737390.wixsite.com/my-site)
  * [NBD](https://notblinddesign.wixsite.com/nbdesign)
  * [UNDEFINED](https://xxzzxh.github.io/)
  * [2021toast](https://perfect-anger-34c.notion.site/2021-Toast-9d52603d5d39409b8337738a551b5df1)
  * [X-Lab](https://x-labby.webflow.io/)
  * [Get1688](https://1175138076.wixsite.com/get1688)
  * [WooHoo](https://richengaa.wixsite.com/woohoo)
  * [OUTLAST Zone](http://121.41.45.236:8001/)

* [Design Manufacture2021](class/fab-2021-zju-dm.md)
  * [The Flash team](https://turatsinzecmu2020.wixsite.com/theflash)
  * [The Crescent](https://bitenotes.wixsite.com/thecrescent)
  * [Estimate](https://lushomo19.wixsite.com/estimate)
  * [Kltkawsar](https://kltkawsar.wixsite.com/website)
  * [Rahmansezad](https://rahmansezad.wixsite.com/astro)

* [Design Engineering2022 ](class/fab-2022-zju-de.md)
  *  [the Red Gang Tailor's](https://nexmaker-fab.github.io/2022zjude1-team5/#/)
  *  [doubleQ](https://nexmaker-fab.github.io/2022zjude1-doubleQ/#/)
  *  [BIG5](https://nexmaker-fab.github.io/2022zjude1-BIG5/)
  *  [SixGod](https://nexmaker-fab.github.io/2022zjude1-SixGod/)
  *  [ALL RIGHT](https://markshaw1234.github.io/2022zjudem-team4.github.io/)
  *  [Peopl3 Drag Per2on](https://nexmaker-fab.github.io/2022zjude1-peopl3-drag-per2on/#/)
  *  [SoFarSoGood](https://nexmaker-fab.github.io/2022zjude1-SoFarSoGood/#/)

* [Design Manufacture2022](class/fab-2022-zju-dm.md)
  * [TECviz](https://nexmaker-fab.github.io/TECviz/#/)
  * [BRAINII MAKERS](https://nexmaker-fab.github.io/2022zjudem-team2/)
  * [NEXEERO](https://sesebak.github.io/NEXEERO_TEAM/#/)
  * [Mavericks](https://nexmaker-fab.github.io/2022zjudem-team4/)

* [Design Engineering mini 2022](class/fab-2022-zju-mini.md)
  * [Four Esigners](https://nexmaker-fab.github.io/2022zjudemini-team1boy/)
  * [Four Vegetables](https://nexmaker-fab.github.io/2022zjudemini-team2/#/)
 
* [Fab Academy 2023  UNNC- NexMaker](doc/Fab/fab2023.md)
  * [Yaolun Zhang](https://fabacademy.org/2023/labs/ningbo/students/yaorun-zhang/)

* [Design Engineering2023 ](class/fab-2023-zju-de.md)
  * [InnoWarm](https://nexmaker-fab.github.io/2023zjude-InnoWarm/#/)
  * [YSD](https://nexmaker-fab.github.io/2023zjude-YSD/)
  * [NOBRAINNOPAIN](https://nexmaker-fab.github.io/2023zjude-NOBRAINNOPAIN/)
  * [Keyidu](https://nexmaker-fab.github.io/2023zjude-keyidu/#/)
  * [10:36](https://nexmaker-fab.github.io/2023zjude-10-36/)
  * [Woodman123](https://design-engineering.littleor.cn/)&[githubpage](https://nexmaker-fab.github.io/2023zjude-Woodman123/)
  * [FiveTigers](https://nexmaker-fab.github.io/2023zjude-FiveTigers/#/)
  * [ORG030](https://nexmaker-fab.github.io/2023zjude-ORG030/)
 
* [Design Manufacture2023](class/fab-2023-zju-dm.md)
  * [The-Dynamic-Seven](https://nexmaker-fab.github.io/2023zjudem-The-Dynamic-Seven/)
  * [7DE](https://nexmaker-fab.github.io/2023zjudem-7DE/)
  * [Tomorrow-s-Wonder-Team](https://nexmaker-fab.github.io/2023zjudem-Tomorrow-s-Wonder-Team/)
  * [Team-novva](https://nexmaker-fab.github.io/2023zjudem-teamnovva/)
  * [INNOGENIUSES](https://nexmaker-fab.github.io/2023zjudem-INNOGENIUSES/)
  * [Gladiators](https://nexmaker-fab.github.io/2023zjudem-Team-Gladiators/)

* [Design Engineering mini 2023](class/fab-2023-zju-mini.md)
  * [Fivevolution](https://nexmaker-fab.github.io/2023zjudemini-hi1/#/)
  * [Fivist](https://nexmaker-fab.github.io/2023zjudemini-hi2/)

* [ZWU-interactivesystem 2024](class/fab-2024-zwu-Interactivesystem.md)
  * [SZPJNF](https://nexmaker-fab.github.io/2024ZWU-IS-1/#/)
  * [Ecobloom](https://nexmaker-fab.github.io/2024ZWU-IS-2-Ecobloom/#/)
  * [Yes or Yes](https://nexmaker-fab.github.io/2024ZWU-IS-3-Yes-or-yes/#/)
  * [watermelon sugar](https://nexmaker-fab.github.io/2024ZWU-IS-4/#/)
  * [BCZG](https://nexmaker-fab.github.io/2024ZWU-IS-5/#/)
  * [None group](https://nexmaker-fab.github.io/2024ZWU-IS-6/#/)

* [ZWU-interactivesystem2+2 2024](class/fab-2024-zwu-Interactivesystem2+2.md)
  * [Mars](https://nexmaker-fab.github.io/2024ZWU-IS-7/#/)
  * [Bunbun](https://nexmaker-fab.github.io/2024ZWU-IS-8-BUNBUN/#/)
  * [BSST](https://nexmaker-fab.github.io/2024ZWU-IS-9-BSST/#/)
  * [4AOL](https://nexmaker-fab.github.io/2024ZWU-IS-10-4AOL/#/)
* [Fab Academy 2025  UNNC- fab academy](doc/Fab/fab2023.md)
  * [Xu Sun](https://fabacademy.org/2025/labs/unnc/students/xu-sun/)
  * [Yaolun Zhang](https://fabacademy.org/2023/labs/ningbo/students/yaorun-zhang/)
* [UNNC-fab course](class/fab-2025-UNNC.md)
  * [Fengyi zhao](https://nexmaker-fab.github.io/2025UNNCFAB-ZHAO/)
  * [Yan bing](https://nexmaker-fab.github.io/2025UNNCFAB-yan/)
  * [Ao Liu](https://nexmaker-fab.github.io/2025UNNCFAB-AO/)
  
## OTHERS

* [XR](XR/readme.md)
  * [Application & solution](XR/application_solution.md)
  * [Brand](XR/brand/aim.md)
  * [How to start](XR/howtostart.md)
  * [Patent](XR/xrultrasoudpatent.md)
  * [Tendency](XR/tendency.md)
  * [Meet up](XR/meetup.md)
  * [Industry Standard](XR/indusrtstandard.md)
  * [Supply chain](XR/supplychain.md)

* [AGV](AGV/readme.md)
  * [Application & solution](AGV/agvapplication.md)
  * [Brand](AGV/agvbrand.md)
  * [How to start](AGV/agvhowtostart.md)
  * [Patent](AGV/agvpatent.md)
  * [Tendency](AGV/agvtendency.md)
  * [Meet up](AGV/agvmeetup.md)
  * [Industry Standard](AGV/agvindusrtstandard.md)
  * [Supply chain](AGV/agvsupplychain.md)
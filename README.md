# NexMaker-Fab

We have online guidebook [www.nexmaker.com](www.nexmaker.com) and offline lab in [NexPCB](www.nexpcb.com)，[ProFabX](www.profabx.com),etc.This lab would arrange training, hardware design, prototype making,project test,etc. We would try to push hardware innovation in our community one by one. In the meantime,We reference [Fablab](http://fabacademy.org/)  to build our lab.   Now we test beta version in our lab. We would open after test the function 


NexMaker academy  offer knowledge and skill about the hardware  development especial prototype stage, and guide participant to practice. During the course , participant can system learn “how to make almost everything”, they would learn project management, mechanical design and manufacture, embedded development ,material, IT, etc. This course is team working, and it is a good opportunity for them to solve some [SDGs](https://sdgs.un.org/) problems.

It's cool if you can give us any advice. We would update the lab one by one. Thank you for your kindly support.




## Student attend the course

![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/2023.png)

<iframe src="https://www.google.com/maps/d/u/0/embed?mid=13td1Pd1stAQKWJ8JkCvZKgJXz3mI13o&ehbc=2E312F" width="100%" height="600"></iframe>




  
## Update recording
* 20200304:build it
* 20200819:add XR summary
* 20210915：add zju2021 team group
* 20210921:email and fablab link
* 202101107：add zju2021 dm team group
* 20220127: add BP in tutorials,rebuild the structure of XR,build the structure of AGV
* 20221201:Fab Lab node
* 20240422: add zwu-interactives sysyems

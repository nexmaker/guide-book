Hi everyone.
The following is our schedule.






| Date        | Course Content           |  
| ------------- | :-----:|	
| 9.28   | Introduction of design engineering | 
|  10.12 | Project management      |  
|10.19 | Computer aided design    |  
| 10.26 |  Q&A  |  
| 11.2 |   3D printing     |  
|11.9     |Computer-controlled cutting | 
| 11.16 |    Midterm presentation ，Q&A    | 
|  11.23    |  Open source hardware and Arduino basic   |  
|  11.30 |  Q&A    |  
|12.7 |   Interface application programming  |  
| 12.14    |    Q&A    |  
| 12.21    |   Q&A   |  
|12.28|  Final presentation     |  
Our evolution method is the following

Daily homework full score is 70

| Course Content       | Full Score           |  
| ------------- | :-----:|	
| Project management  | 10   | 
| Computer aided design  |  10    |  
|   3D printing  |  10  |  
|   laser cutter |  10  |  
| Arduino   | 20  |  
|  Interface application programming  |  10   |  



Final presentation full socre is  30 


| Evolutation aspect       | Full Score           |  
| ------------- | :-----:|	
| SDGs  |  6  | 
| Innovation |6      | 
| Market analysis | 6     | 
| Key tech analysis | 6     | 
| Material and how to make it | 6     | 




#### [TECviz](https://nexmaker-fab.github.io/TECviz/#/)

| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management  |~~1.wait teammember information~~<br> ~~2.check link(Introduction to Design Engineering,introduce team ,how we build our webpage)~~<br>~~3.how to start every step~~ <br>~~4.add some screenshot for key step~~<br>~~5. no copy for homework~~<br>~~6.check the layout web(picture with words)~~<br>~~7.delete hobbies and teamrole if no infromation in teammember introduce~~<br>8.express how to clone github repository, how to run git,how to open terminal for the first time<br>~~9. _sidebar.md should be your onw infromation not copy~~<br>10.mention how to pull and push to github|20230403|8| 
| Computer aided design |~~1.detail screenshot show how to design every step at least one part(including how to the command panel);~~<br>~~2.bigger size picture not so small~~<br>~~3.show how to assembly & motion~~<br>~~4.everyone need at least one part~~<br>~~5.engineering drawing~~<br>~~6.simple introduce anther CAD software(for example you use solidworks)~~<br>~~7.method to add solidworks 3d model in web~~<br>8.mention how to make engineering draw and motion |20230331|8| 
|  3D printing  |~~1.checking the layout of picture ,the size too small~~<br>~~2.how to practice in software~~<br>~~3. how to translate data from software to machine~~ <br>~~4.machine setting~~ <br>5.how to take model out and delete useless materail(support, raft)<br>~~6.is this your own working for final assembly~~<br>~~7.show how to control machine~~|20230403|7| 
| Computer-controlled cutting |1.connect with arduino <br>2.show design detail expecial kerf<br>3. mention sth about kerf |20230403|7| 
|  Arduino control| every project need BOM,thinkercad connect,coding and real physical runing picture or gif|20230331|14| 
| Interface application programming ||20230403|7| 
| SDGs ||20230404|5| 
| Innovation ||20230404|4| 
| Market analysis ||20230404|4| 
| Key tech analysis ||20230404|5| 
| Material and how to make it ||20230404|6| 






#### [BRAlNII   MAKERS](https://nexmaker-fab.github.io/2022zjudem-team2/)
| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management  |~~1.webpage layout;~~<br>~~2.about button;~~<br>~~3.teammember's picture and link;~~<br>~~4.highlight coding;~~<br>~~5.method for language and step1-step3~~<br>~~6.delete assessment~~<br>~~7.check the fr button for language~~<br>~~8.checking the chinese for homework name(some of them no transalation or wrong translation)~~<br>~~9.no chinese version for laser cutter,processing~~|20230403|9| 
| Computer aided design |~~1.detail for one design(part, assemble,motion,colar and materail)~~<br>~~2.some another CAD software introduce~~<br>~~3.enlarge picture,picture not clear~~<br>~~4. need assemble~~<br>~~5.need motion~~<br>~~6.small vidoe use gif better~~<br>~~7.constrain all sketch in "Step 2:"all the line would go from blue to black~~<br>~~8. explain the command how to extrude from sketch~~|20230403|10| 
|  3D printing  |~~1.software new setting~~<br>~~2.printing location method~~<br>~~3.clean support method~~<br>~~4.software and 3d printer background put in reference ,no need in class2 and class3~~<br>~~5.how to change the size in software for version 2~~<br>~~6.gif for assemble motion~~<br>~~7. show how to put the final design data in slice software~~<br>~~8.post process method at least remove clean support~~<br>~~9.show how to control machine~~ |20230404|9| 
| Computer-controlled cutting |~~1.add test data for cutting~~<br>~~2.show sth assembly parameter~~<br>~~3.some assembly  relation for arduino~~|20230331|10| 
|  Arduino control|~~1.checking heading size to make it easy to read~~<br>~~2. all coding from picture to text~~|20230403|18| 
| Interface application programming ||20230331|9| 
| SDGs ||20230404|6| 
| Innovation ||20230404|5| 
| Market analysis ||20230404|5| 
| Key tech analysis ||20230404|6| 
| Material and how to make it ||20230404|6| 

#### [NEXEERO](https://sesebak.github.io/NEXEERO_TEAM/#/)
| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management  |~~1.edit social media link~~<br>~~2.add personal picture~~ <br>~~3.column for introduction~~<br>~~4.update language setting~~<br>~~5 no space in name~~<br>~~6.change faq page~~<br>~~7.sidebar layout ~~<br> ~~8.faq,everyproblem need atleast one screenshot~~<br>~~9. "5. Add sidebar and navbar" in document not copy~~, siderbar's link has problem<br>~~10.show detail coding for "C. SET UP TEAMINTRO"~~<br>~~10.checking the launguage for homework ,need corresponding document for everything if you have language choice~~|20230403|9| 
| Computer aided design |~~detail for design,material step by step at least one case~~ <br>~~2.motion and assemble~~<br>~~3. drawing~~|20230403|9| 
|  3D printing  |~~1.checking picture~~ <br>~~2.show how to setting in slice software~~<br>~~3. show how to control machine~~ <br>~~4. show how to post processing~~|20230403|9| 
| Computer-controlled cutting |1.need assembly with Arduino<br>2.show design detail expecial kerf<br>~~3.assembly the model with gif~~|20230331|7| 
|  Arduino control| ~~check assessment ,need some of LED, motor,sensor,screen,but now just LED~~|20230403|16| 
| Interface application programming |~~1.do own working except class working and snake game~~<br>~~2.the case processing connect with arduino~~|20230331|7| 
| SDGs ||20230404|6| 
| Innovation ||20230404|4| 
| Market analysis ||20230404|4| 
| Key tech analysis ||20230404|4| 
| Material and how to make it ||20230404|4| 

#### [Mavericks](https://nexmaker-fab.github.io/2022zjudem-team4/#/)
| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management  | ~~1.web build method~~<br>~~2.check "our version" and "language"~~<br>~~3.delete useless link for aboutus~~<br>~~4.check the picture of about us~~<br>~~5.check the 3d printer information in sidebar~~<br>6.how to pull and push to github from local<br>~~7.checking the launguage for homework ,need corresponding document for everything if you have language choice,no Arabic in navbar but document mention it~~<br>8. wrong link for "about us" in navbar|20230403|8| 
| Computer aided design |~~1.keyword simple introduce every step~~ <br>~~2.assemble ~~<br>3.motion<br>4.drawing <br>~~5.another CAD software introduce~~<br> ~~6. check the link for Autodesk~~|20230331|8| 
|  3D printing  |~~1.check the assessment for detail~~<br>2.how to remove support |20230331|8| 
| Computer-controlled cutting |~~1.wrong link for picture~~<br>~~2.big picture size for better read~~<br>3.assemble with arduino<br>4.parameter especial kerf show in CAD|20221221|7| 
|  Arduino control|~~1.LCD screen need wire connect picture and final result~~<br>~~2.check the size of video~~|20230331|16| 
| Interface application programming |1.expresss project function<br>~~2.do one your own project~~<br>~~3.the case processing connect with arduino~~|20230403|9| 
| SDGs ||20230404|6| 
| Innovation ||20230404|5| 
| Market analysis ||20230404|4| 
| Key tech analysis ||20230404|5| 
| Material and how to make it ||20230404|5| 
******
#### update
* 20221007:build page;
* 20221026：update  schedule;
* 20221121：update zju2022manufacure v1 condition;
* 20230403:final evolution for daily homework；
* 20230404:final evolution for final project；




#### Reference information
1. tecviz
2. Brainii Makers  
   
3. NEXEERO
   * The term 'npm' is not recognized as the name of a cmdlet, function, script file, or operable program
   *  LANGUAGE
   *  teamsetting for html
4. Mavericks
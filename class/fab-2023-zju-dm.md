Hi everyone.
The following is our schedule.we would do it step by step.



| Date        | Course Content           |  
| ------------- | :-----:|	
| 0919      | Introduction of design engineering | 
| 0926      | Project management      |  
| 1010 | QA    |   
| 1017     |Computer aided design| 
| 1024 |   Open-source hardware and Arduino basic     | 
| 1031     | Arduino (output devices)  | 
|1107|  Arduino (input devices)   | 
|1114   |3D printing  |  
| 1121 |  Interface application programming  |  
| 1128     | Midterm presentation    |  
| 1205|   Computer-controlled cutting |   
| 1212    |Arduino IOT    |  
| 1219 |  Intelligent Materials    |  
| 1226      |Patent application |  
| 0102 |  Q&A     |  
| 0109    | Final presentation  |  





Our evolution method is the following

Daily homework full score is 70

| Course Content       | Full Score           |  
| ------------- | :-----:|	
| Project management  | 10   | 
| Computer aided design  |  10    |  
|   3D printing  |  10  |  
|  Computer-controlled cutting |  10  |  
| Arduino (10)+Arduino IOT(10)  | 20  |  
|  Interface application programming  |  10   |  



Final presentation full socre is  30 


| Evolutation aspect       | Full Score           |  
| ------------- | :-----:|	
| SDGs  |  6  | 
| Innovation |6      | 
| Market analysis | 6     | 
| Key tech analysis | 6     | 
| Material and how to make it | 6     | 








####  [The-Dynamic-Seven](https://nexmaker-fab.github.io/2023zjudem-The-Dynamic-Seven/)

| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management      |~~1.wrong link for github &githubdesktop~~<br>~~2.how to write the first document(tilte,header, content,picture)~~<br>3.~~how to collaborator team,~~you repeat it <br>~~4.use coding not picture~~<br>~~5.setting the environment~~<br>~~6. the relationship for two repository~~<br>~~delete useless link and information in personal webpage~~|20231107|10| 
| Computer aided design  |~~1.the full interface<br>2.vidoe from screenshot not use mobile phone~~<br>~~[3. check the assessment for more detail](https://www.nexmaker.com/doc/2cad/Assessment.html)~~<br>~~4.3d model live~~<br>~~5.engineering drawing~~<br>~~6. plugins~~<br>~~screenshot vidoe not mobile phone vidoe~~<br>~~7.Past the link on the website and We add gif with this code~~<br>~~9. parameter design~~<br>~~10.practice picture can be high quality gif~~<br>~~11.simple introduce~~<br>~~12.[practice for Automated Modeling](https://help.autodesk.com/view/fusion360/ENU/?guid=SLD-AUTOMATED-MODELING)~~<br>~~13.introduce other software~~ |20240102|10| 
|3D printing     |~~1.find new reseach or application~~<br>~~2.how to clean support~~<br>~~3.gcode~~you can try to add CNC Simulatore link and reference information|20240107|10| 
|  Arduino    |~~1.need wire horizon and vertical~~<br>~~2.other project introduce~~ |20240102|10| 
|  Interface application programming  |~~1.enivornment and basic install information<br>2.other software~~ <br>3. need resistant(including data) for LED Control with Hand Gestures|20240107|9| 
| Computer-controlled cutting |~~1.laser standard~~<br>~~2.materail~~<br>~~3.laser cutter with arduino(arduino need bolt fix with acrylic)~~<br>~~4.measure kerf~~<br>~~5. some picture's degree~~|20240108|10|  
|  Arduino IOT   | ~~1.need vpn or not~~|20230107|9| 
| Final presentation  |  ||soon| 


#### [7DE](https://nexmaker-fab.github.io/2023zjudem-7DE/)

| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management      |~~1.how to open terminal~~<br>~~2.how to make sidebar with hight coding not picture~~<br>~~3.how to write the first document(tilte,header, content,picture)~~<br>~~4.explain the detail of index.html~~<br>~~5.how to collaborator team~~<br> ~~6.coding with word not just picture~~<br>~~7.the relationship fold and file~~<br>~~8.check collaborator relationship~~|20231204|9| 
| Computer aided design  |~~1.vidoe from screenshot not use mobile phone~~<br>~~[2. check the assessment for more detail](https://www.nexmaker.com/doc/2cad/Assessment.html)~~<br>~~3.Animation more detail~~<br>~~4. picture size and high quality picture~~<br>~~5. high quality video for "Daphne Isatou Timbo's problem"<~~br>~~6. joint~~<br>~~7.engineering drawing~~<br>~~8.parameter design~~<br>~~9.[practice for Automated Modeling](https://help.autodesk.com/view/fusion360/ENU/?guid=SLD-AUTOMATED-MODELING)~~<br>~~10.introduce other software~~<br>11.confirm all sketch constrain|20240107|9| 
|3D printing     |~~1.add reference link~~<br>~~2.parameter table~~|20240107|10| 
|  Arduino    | ~~1.need your own case~~<br>~~2.other project introduce~~ |20240102|9| 
|  Interface application programming  |~~need your own case~~ |20240102|9| 
| Computer-controlled cutting|~~1.arduino with lasercutter~~<br>~~2.material picture~~|20240107|10|  
|  Arduino IOT   | |20230108|9| 
| Final presentation  |  ||soon| 


#### [Tomorrows-Wonder-Team](https://nexmaker-fab.github.io/2023zjudem-Tomorrow-s-Wonder-Team/)

| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management  |~~1.member's information~~<br>~~2.school' link<br>~~~~3.home link button and function~~<br>~~4. no link for project work~~<br>~~5.how to collaborator team(add one srceenshot),need result~~<br>~~6.rough idea for final project~~<br>~~7.more detail for web build,with picture~~<br>8.~~how to write document for one page,and high quality screenshot~~<br>~~9.the relationship for two repository~~<br>~~10.how to open terminal~~<br>~~11. sidebar~~<br>~~12.highlight coding not in ""~~<br>13.~~Docsify Installation method need more detailm,delete docisfy~~<br>~~14.mention the folder and file relationship~~ <br>15.~~highlight AutoHTML and explain detail for "figma"  design and develop mode,and detail for one html,highlight coding for index.html~~<br>~~16.mention your final project~~<br>~~17.link in personal webpage~~<br>~~18how to link in figma~~|20231226|10|
| Computer aided design  |~~1.mention the position of gear with screenshot~~<br>~~2.mention the cylinder before gear~~<br>~~3.detail for motion,~~<br>~~4.Simple parameter design practice;~~<br>~~5.Try one plugins and used in your design;~~<br>~~6.Engineering Drawing for on component at least；~~<br>~~7.[practice for Automated Modeling](https://help.autodesk.com/view/fusion360/ENU/?guid=SLD-AUTOMATED-MODELING)~~<br>~~8.gear assemble~~<br>~~9.revolute1 angle is different with revolute2~~|20240108|10| 
|3D printing     |~~1. layout and paramenter setting in slice software~~<br>~~2.how to take the model out~~<br>~~3.how to assemble the model~~<br>~~4.machine choice~~<br>~~5.reference link for case~~|20240108|9| 
|  Arduino    |~~1.need own case~~<br>~~2.more detail for MAX7219 Dot Matrix (4 in 1)~~<br>~~3. hardware connection~~ <br>~~4.code explain~~<br>~~5.other project introduce~~<br>improve pic &vidoe size<br>~~6. wire horizon and vertical and colorM~~<br>~~7.I2C,~~|20240108|9| 
|  Interface application programming  |~~NEED own case~~  |20231226|8| 
| Computer-controlled cutting  |~~1.kerf~~<br>~~2.check assessment and add information~~|20240108|10|  
|  Arduino IOT   |need input &output  |20240108|8| 
| Final presentation  |  ||soon| 



#### [Team-novva](https://nexmaker-fab.github.io/2023zjudem-teamnovva/)

| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management      |~~1.no information about homework record~~<br>~~2.how to collaborator team~~ <br>~~3.gabrirla's personal's picture has problem during loading~~<br>~~4. jiko's web page~~<br>~~5.wrong link in tables of contents~~<br> ~~6.no final project rought~~<br>~~7.chinese webpage~~<br>~~8.the front and empty space in design part strange~~<br>~~9. more detail for design in figma ,focus on home page for frame and component~~s<br>~~10.picture  in development~~<br>~~11.deployment~~<br>~~12.all fork  from pwmp.github.io to nexmaker team's repository~~<br>~~13. navbar link for personal webpage~~  |20240102|10| 
| Computer aided design  |~~[1. check the assessment for more detail](https://www.nexmaker.com/doc/2cad/Assessment.html)~~<br>~~1.sketch need constrain~~<br>~~2.detail design for one model at least~~<br>3.joint<br>~~4.Design4,Design5,Fusion360,Inventor~~<br>5.Simple parameter design practice;<br>~~6.test contact set/motion link and show gif;~~<br>~~7.Try one plugins and used in your design;~~<br>8.Engineering Drawing for on component at least；<br>~~9.Simple introduce another CAD software or experienc~~<br>10.[practice for Automated Modeling](https://help.autodesk.com/view/fusion360/ENU/?guid=SLD-AUTOMATED-MODELING)<br>CAD IN TABLE OF CONTENTS is wrong|20240109|9| 
|3D printing     |~~your own model~~|20240108|10| 
|  Arduino    | ~~1arduino output for switch~~|20240102|7| 
|  Interface application programming  |~~1.coding can't show all information~~<br>~~2.checking assessment again~~|20240108|10| 
| Computer-controlled cutting   |~~1.picture can't load~~|20240108|10|  
|  Arduino IOT   | check the homework again|20240108|3| 
| Final presentation  |  ||soon| 


#### [INNOGENIUSES](https://nexmaker-fab.github.io/2023zjudem-INNOGENIUSES/)

| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management      | ~~1.the relationship for link(especial social media)~~<br>~~2.how to build the web~~<br> ~~3.how to write the first document(tilte,header, content,picture)~~<br>~~4.how to collaborator team~~<br>~~5. add the link for web tools~~<br>~~6.delete useless link in personal webpage~~<br>~~7.show all index.html with coding~~<br>~~8.show detail for folder and file~~<br>~~9.how to make the taskbar~~<br>~~10.the teammember's template,need correspond link~~<br>~~11.two repositories relationship~~<br>12.picture size|20240108|9|
| Computer aided design  |~~1.highlight sketch and extrude logo~~<br>~~2.detail for  Text sketch and Emboss tool~~<br>~~3. parameter design,all data show in table or picture~~<br>~~4.joint test~~<br>~~5.motion test result(upload high quality 2videos in github)~~<br>~~6.engineering drawing~~<br> ~~7. fusion 360 plugin test~~<br>~~8.[practice for Automated Modeling](https://help.autodesk.com/view/fusion360/ENU/?guid=SLD-AUTOMATED-MODELING)~~<br>~~9.Simple introduce another CAD software or experienc~~<br>~~10.sketch constrain~~<br>11.picture scale is wrong|20240108|9| 
|3D printing     |~~1.postprocess~~<br>~~2.gcode~~<br>~~3.new application~~<br>~~4.every picture need reference link if reference from other platform~~|20240108|9| 
|  Arduino    |~~1.need own case~~<br>~~2.checkpicture(gif&video) quality~~<br>~~3.need coding not just picture~~<br>~~4.other opensource case~~<br>~~5.for light Seeking Mobile Robot ,no reference link ,coding,~~ ~~picture scale is wrong~~<br>~~6.need iot project~~  |20240108|9| 
|  Interface application programming  |~~1.reference information need link~~ <br>~~2.picture and video need high quality~~|20240108|9| 
| Computer-controlled cutting   | reference information need link|20240106|10| 
|  Arduino IOT   | |20240108|8|  
| Final presentation  |  ||soon| 


#### [Gladiators](https://nexmaker-fab.github.io/2023zjudem-Team-Gladiators/)

| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management  |~~1.how to build the web not in pdf~~<br> ~~2.how to write the first document(tilte,header, content,picture)~~<br>~~3.how to collaborator team~~<br>~~4.final project~~<br>~~5.check the link for gallery and semimars~~<br>~~5.picture with high quality~~<br>~~6.wrong infromation in vscode ,need total picture~~<br>~~7.language~~<br>~~8. don't need  translation for coding~~<br>~~9. sidebar and navbar~~<br>~~10. wrong for "设置会员"in Chinese~~<br>~~11.repository relationship~~|20231219|9|
| Computer aided design  |~~1.Here is the steps to install fusion 360,need more detail and picture~~<br>~~2.highlight sketch and extrude logo~~<br>~~3.Construct Section (Tangent) & Modify (Fillet)~~<br>~~4.joint detail~~<br>~~5.check picture and video size~~<br>~~6.plugin used in design~~<br>~~7.how to design drawing~~<br>~~8.parameter design~~<br>~~9.test contact set/motion link and show gif~~<br>~~10.[practice for Automated Modeling](https://help.autodesk.com/view/fusion360/ENU/?guid=SLD-AUTOMATED-MODELING)~~|20240107|9| 
|3D printing     |~~1.more parameter~~<br>~~2. show the result for your design~~<br>~~3.the nut how to assemble~~<br>~~4.find new reseach or application~~ |20240102|10| 
|  Arduino    | ~~1.tinkcader+coding+physical result~~<br>~~2.other opensource case~~ |20231219|9| 
|  Interface application programming  |~~show own case or reference link~~ |20240108|10| 
| Computer-controlled cutting   |1.laser cutter with arduino together<br>~~2.picute with reference link~~<br>~~3.picture size~~<br>~~4. some picture not show "Laser-cut Lamp with Arduino"~~|20240102|9|  
|  Arduino IOT   |~~1. picture size(recommand to enlarge width:100%) and scale~~<br>~~2.tinkercad drawing~~<br>~~3.nexmaker, not nextmaker~~<br>~~4.coding with highlight not picture~~<br>~~5.how to install library~~ |20240109|9| 
| Final presentation  |  ||soon| 

******
#### update
* 20230925:build page;






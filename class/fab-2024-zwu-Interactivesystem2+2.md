# Basic introduce
Hi everyone ,this semester we would learn interactivesystem together.
The following is our schedule.we would do it step by step.

Interactivesystem workshop offer knowledge and skill about the hardware  development especial prototype stage, and guide participant to practice. During the course , participant can system learn “how to make almost everything”, they would learn project management, mechanical design and manufacture, embedded development ,material, IT, etc. This course is team working, and it is a good opportunity for them to solve some [SDGs](https://sdgs.un.org/) problems.

We have online guidebook [www.nexmaker.com](www.nexmaker.com) . We would try to push hardware innovation in our community one by one. In the meantime,We reference [Fablab](http://fabacademy.org/)  to build our lab.  Nov 30,2022,Bob success become [Fab Lab instructor](http://fabacademy.org/nodes/requirements.html#instructors-application-process) and Fab Lab Ningbo-NexMaker become[ Fab Academy 2023 node](https://fabacademy.org/2023/labs.html),so we would launch Fab Academy 2023 with the help of some friends.


# Timetable
| Date | Course Content           |  
| ------------- | :-----:|	
| 0425 | Introduction of design engineering | 
| 0428 | [Project management](doc/1projectmanage/Assessment1.md)      |  
| 0509 | [Open-source hardware and Arduino output ](doc/5arduino/assessment.md) |   
| 0516 | pre presentation & QA     | 
| 0521 | [Arduino input ](doc/5arduino/assessment.md)   | 
| 0523 | [Processing(Interface application programming)](doc/11Interface-application-programming/Assessment.md)|  
| 0528 | [Computer aided design](doc/2cad/Assessment.md)|
| 0530 | QA |
| 0604 | [ Material](doc/12material&tool/assessment.md) | 
| 0606 | Midterm presentation    |  
| 0611 | [3D printing](doc/3_3dprinter/assessment.md)  | 
| 0613 | [Computer-controlled cutting](doc/6laser_cutter/Assessment.md)  |    
| 0618 | [IOT](doc/10IOT/Assessment.md)     |  
| 0620 | QA     |  
| 0625 | QA     |  
| 0627 | Final presentation  |  

# Evolution
Our evolution method is the following

Daily homework full score is 60

| Course Content       | Full Score  |  
| ------------- | :-----:|	
| [Project management](doc/1projectmanage/Assessment1.md)             | 10   | 
| [Arduino output](doc/5arduino/assessment.md)                        | 5    | 
| [Arduino input](doc/5arduino/assessment.md)                         | 5    | 
| [Processing](doc/11Interface-application-programming/Assessment.md) |  5   |  
| [Material](doc/12material&tool/assessment.md)                       |  5   |  
| [Computer aided design](doc/2cad/Assessment.md)                     |  10  |  
| [3D printing ](doc/3_3dprinter/assessment.md)                       |  10  | 
| [Computer-controlled cutting](doc/6laser_cutter/Assessment.md)      |  5   | 
| [Arduino IOT](doc/10IOT/Assessment.md)                              |  5   |  





#### [Mars](https://nexmaker-fab.github.io/2024ZWU-IS-7/#/)
| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management      |~~1.change log in &sign up~~<br>~~2.the button for "about us" need information~~<br>~~3.no homework~~<br>~~4. Team Members'link in navbar is wrong ;Team Members' picture is wrong,add personal webpage（own repository， not in team repository)~~<br>~~5. need final project's idea~~<br>6,how to build the web|0602|7 | 
|  Arduino output   |~~1.the size of picture recommand the same~~ <br>2.update the information  from wiki<br>3.detail for stepper motor<br>4.tinkercad connect<br>~~5.gif for result~~<br>~~6.highlight the coding~~|0602|3| 
|  Arduino input  | 1.tinkercad connect<br>2.gif for result<br>~~3.highlight the coding~~ |0602|3| 
|  Interface application programming  |1.check picture |0602|4| 
| Material  |~~1.quality of picture~~<br>~~2.new material~~<br>plastic two picture<br>metal postprocess<br>plastic postprocess|0618|4| 
| Computer aided design  |~~1.cosntrain all sketch~~<br>~~2.extrude~~<br>~~3.check picture quality~~<br>4.assemble<br>~~5.automated model~~<br>~~6. drawing~~<br>~~simlar software~~|0625|8| 
|3D printing     |~~1.peek(temperature and force)~~<br>2.machine setting<br>~~3. check wall thickness~~<br>~~4.preview~~<br>~~5. machine operation~~<br>~~6. postprocess method~~<br>7.gcode<br>~~8.cura link~~|0625|9| 
| Computer-controlled cutting||0625|4|  
|  Arduino IOT   | |0625|4| 

#### [Bunbun](https://nexmaker-fab.github.io/2024ZWU-IS-8-BUNBUN/#/)
| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management      |~~1.use member's name replace menber1,member2,etc~~<br>~~2.how to define sidebar(need the new version)in index.html~~<br>~~3.write a simple document with markdown ,or html~~<br>~~4. the main relation for file and fold in github~~<br>~~5.need css detail~~<br>~~6. the setting for picgo~~<br>~~7.how to use git to clone,pull push repositroy~~<br>~~8.add personal webpage（own repository， not in team repository~~<br>~~9. high light all index.html coding~~ |0602|10| 
|  Arduino output   | ~~1.the link for Orange Pi~~<br>~~2.check the Example for LED~~<br>~~3.homework case need own not from class~~  |0601|5| 
|  Arduino input  | ~~1.tinkercad connect wrong~~<br>~~2. need your own case~~ |0602|5| 
|  Interface application programming  |~~1.check processing coding(portname)~~<br>~~2.need gif show result~~<br>~~3. mention the reference link~~<br>~~4. show the result of example~~<br> ~~5. from Keyboard interaction  to mouse interaction~~|0601|5| 
| Material  |~~1.material step by step~~<br>~~2. link~~<br>~~3. ti series and postprocess~~<br>~~4.plastic postprocess~~<br>~~5.wood safety~~<br>~~5.bolt and nut~~|0625|5| 
| Computer aided design  |~~1. constrain all sketch~~ <br>~~2.how to extrude,assmeble and motion~~<br> ~~3.Simple parameter design practice;~~<br>~~4.Try one plugins and used in your design~~<br>5.Engineering Drawing for on component at least；<br>~~6.Practice for Automated Modeling~~<br>~~7.show the result of automated model~~|0602|9| 
|3D printing     |~~1.FDA~~<br>~~2.lightweight~~<br>~~3.automobile~~<br>~~4.machine setting in software~~<br> ~~5.postprocess~~|0625|10| 
| Computer-controlled cutting|~~1.SOP~~<br>~~2.drilling machine~~<br>~~3.paper for lasercutter~~|0625|4|  
|  Arduino IOT   | |0625|4| 

#### [BSST](https://nexmaker-fab.github.io/2024ZWU-IS-9-BSST/#/)
| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management      |~~1.ignore telephone number in web~~<br>~~2. show coding with text not picture~~<br>~~3. how to write the one of webpage~~<br>~~4.web can't run~~<br>~~5.team member's picture not working~~<br>~~6. need high quality picture~~<br>~~7.how to set the home page~~<br>~~8. picture left or center~~<br>~~9.show how to make button,link and picture in figma~~|0602|10 | 
|  Arduino output   | ~~need own case to show your practice not class's case~~ |0602|5| 
|  Arduino input  | ~~1.check the picture~~<br>~~2. need own case to show your practice not class's case~~|0602|4| 
|  Interface application programming  |~~1.go to one page(don't connect with arduino input)~~<br> ~~2. need circuit connect~~<br>3. read the detail for assessment |0602|3| 
| Material  |1.~~add picture~~ and link <br>~~2. material for final project~~<br>~~3.how to run for laser holographic material~~<br>~~4.change the position for wood workflow~~<br>~~5.keyword for final project material~~|0625|4| 
| Computer aided design  |~~check the assessment carefully~~<br>~~2.need own case to show your practice not class's case~~<br>~~3.extrude~~<br>4.assemble<br>~~5.Engineering Drawing~~<br>6.automated model|0625|8| 
|3D printing     |1.need application<br>2.need model and then setting<br>~~3.check flow setting~~ <br>~~4.check the size of machine~~ <br>~~5.the result of slice~~<br>~~6.Use 3D printer to manufacture it~~<br>~~7.improve painting and detail~~<br>~~8.Try to read gcode~~|0625|9| 
| Computer-controlled cutting|1.safety picture<br>~~2.CNC picture~~<br>3.check the machine ,material<br>4.lasercutter|0625|3|  
|  Arduino IOT   | |0625|3| 

#### [4AOL](https://nexmaker-fab.github.io/2024ZWU-IS-10-4AOL/#/)
| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management      |~~1.can't click navbar for team working and personal web~~<br>~~2.add personal webpage（own repository， not in team repository~~<br>~~3.no homework~~<br>~~4.Froyo has two picture~~<br>~~5.the problem for 4,5,6,~~<br>~~6. picture can't load~~<br>~~7.coding need highlight not picture~~<br>~~8.picture need high quality~~<br>~~9.navbar in assessment  homework is not working~~|0601|10 | 
|  Arduino output   |~~need your own case and show step by step~~  |0601|4| 
|  Arduino input  | n~~eed tinkercad connect~~<br>~~2.check the connect with sensor~~ |0602|4| 
|  Interface application programming  | ~~check tinkcaed connec~~|0602|5| 
| Material  |~~1.need more detail about milk plastic~~<br>~~2.check Fire and heat insulation~~<br>~~3. manufacture method(result)~~<br~~>4.delete"Plastic disposal methods"~~<br>~~5.Acrylic pciture~~<br>~~6.pc pipe~~|0625|5| 
| Computer aided design  |~~1constrain all sketch(from blue to black)~~<br>~~2.parameter design (some of parameter has relationship for example 1mm,not just give the data)~~<br>~~3,need engineering drawing~~|0625|10| 
|3D printing     |~~1.new picture to new technology~~<br>~~2.change the position of slice screenshoot~~<br>~~3.infill~~<br>~~4. machine setting~~<br>~~5. material~~<br>~~6.gcode~~|0625|9| 
| Computer-controlled cutting|~~cnc~~ |0625|3|  
|  Arduino IOT   | |0625|3| 



******
#### update
* 20240420:build page;
* 20240626 finish






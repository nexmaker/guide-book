Hi everyone.
The following is our schedule for 8 weeks，start from 9 Nov.

| Date        | Course Content           |  
| ------------- | :-----:|	
|11.9     |Introduction of design engineering | 
| 11.16 |   Project management   | 
|  11.23    |   Computer aided design   |  
|  11.30 |  3D printing & laser cutter    |  
|12.7 |  Arduino    |  
| 12.14    |   Interface application programming    |  
| 12.21    |   Q&A   |  
|12.28|  Final presentation     | 

## Our evolution method is the following

Daily homework full score is 70


| Course Content       | Full Score           |  
| ------------- | :-----:|	
| Project management  | 15   | 
| Computer aided design  |  10    |  
|   3D printing & laser cutter |  20  |  
| Arduino   | 15  |  
|  Interface application programming  |  10   |  


Final presentation full socre is  30 


| Evolutation aspect       | Full Score           |  
| ------------- | :-----:|	
| SDGs  |  6  | 
| Innovation |6      | 
| Market analysis | 6     | 
| Key tech analysis | 6     | 
| Material and how to make it | 6     | 



## [Four Esigners](https://nexmaker-fab.github.io/2022zjudemini-team1boy/)

| Course Content   |  Question        |  Update time   | Condation | 
| ------------- | :-----:|	 :-----:|	 :-----:|	
|Introduction of design engineering | | |done | 
|   Project management   | ~~1.add hexo link in page;~~<br>~~2.Simple introduce how to run git during beginning;~~<br>~~3. introduce the opertation of"-config.yml";~~<br>~~4.Mention how to clone to local and push to github;~~<br>~~5.show how to setting navbar;~~<br>~~6.Show the layout of file  in github especially the main working for homework and final project；~~<br> ~~7.checking the layout of "projects" page~~  | 20230306|13 | 
|   Computer aided design   |~~1.Attach the download link for data；~~<br>~~2.show the gif for the result of "Motion Animation Procedure";~~ <br>3.Simple parameter design practice;<br>~~[4. Try one plugins and used in your design"utilities-addins"](https://apps.autodesk.com/FUSION/en/Home/Index)~~|20230307 |9 |  
|  3D printing & laser cutter    | ~~1.laser cutter design and manufacture project~~<br>~~2.connect with arduino;~~<br>~~3.[checking the kerf](https://www.nexmaker.com/doc/6laser_cutter/Design_guide.html) not error;~~<br>~~4. show clear picture for laser cutter project"soft robot"~~|20230308 |20 |  
|  Arduino    | ~~1.in case2 you have case1 picture~~  |20230306| 13| 
|   Interface application programming    |~~1.show the webpage how to install p5js;~~<br>~~2.show the connection of arduino in thinkercad~~ |20230307 |9 |  
|  Final presentation     | SDGs:4 ;<br>Innovation:4;<br>    Market analysis:6; <br>  Key tech analysis:3;  <br>  Material and how to make it:4|20230308 |21 |  
















 ## [Four Vegetables](https://nexmaker-fab.github.io/2022zjudemini-team2/#/)
 
 | Course Content   |  Question        |  Update time   | Condation | 
| ------------- | :-----:|	 :-----:|	 :-----:|	
|   Project management   |1.English version better than Chinese version；<br> ~~2.nodejs link and how to use git to check node version;~~<br>~~change from "—sidebar.md" to "_sidebar.md" ;~~<br>~~3.more coverypage information;~~<br>~~4.git clone before setting docsify in vscode;~~<br>~~5.more detail during push to github;~~<br>~~6.show how to public webpage;~~<br>~~7."打开github终端并键入下列代码：" from github to git~~ <br>~~8.check the sidebar real infromation not copy;~~<br>~~9.checking the coverpage real information not copy;~~<br>~~10. check the clone information not just copy;~~<br>~~11.why need GitHub Enterprise Cloud~~ |20230308 | 12| 
|   Computer aided design   | ~~1.design detail in one model;~~<br>~~2.assemble method;~~<br>~~3.motion detail;~~<br>~~4.engineering drawing;~~<br>5.Simple parameter design practice;<br>~~[6. Try one plugins and used in your design"utilities-addins"](https://apps.autodesk.com/FUSION/en/Home/Index)~~ | 20230308|7 |  
|  3D printing & laser cutter    | 1.software parameter for 3D printing machine；<br>2.machine information for laser cutter；<br>3. from "准确度测试"to kerf"激光直径"；<br>4. laser cutter make assemble model;<br>laser cutter model assemble with arduino project  | 20230307| 20|  
|  Arduino    |  ~~1.repair the coding for example;~~<br>~~2.show the result for ultra sensor~~;<br>~~3. check "lcd_display() "for Ultrasonic sensor~~ |20230308 |13 | 
|   Interface application programming    |~~1.checking the coding for arduino ,Serial.write~~ |20230308 |9|  
|  Final presentation     | SDGs:5;  <br>    Innovation:3;  <br>    Market analysis:6;   <br>   Key tech analysis:3;  <br>   Material and how to make it:3;|20230308 | 20|   


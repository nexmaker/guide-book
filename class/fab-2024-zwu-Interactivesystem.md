# Basic introduce
Hi everyone ,this semester we would learn interactivesystem together.
The following is our schedule.we would do it step by step.

Interactivesystem workshop offer knowledge and skill about the hardware  development especial prototype stage, and guide participant to practice. During the course , participant can system learn “how to make almost everything”, they would learn project management, mechanical design and manufacture, embedded development ,material, IT, etc. This course is team working, and it is a good opportunity for them to solve some [SDGs](https://sdgs.un.org/) problems.

We have online guidebook [www.nexmaker.com](www.nexmaker.com) . We would try to push hardware innovation in our community one by one. In the meantime,We reference [Fablab](http://fabacademy.org/)  to build our lab.  Nov 30,2022,Bob success become [Fab Lab instructor](http://fabacademy.org/nodes/requirements.html#instructors-application-process) and Fab Lab Ningbo-NexMaker become[ Fab Academy 2023 node](https://fabacademy.org/2023/labs.html),so we would launch Fab Academy 2023 with the help of some friends.



# Timetable
| Date | Course Content           |  
| ------------- | :-----:|	
| 0422 | Introduction of design engineering | 
| 0429 | [Project management](doc/1projectmanage/Assessment1.md)      |  
| 0506 | [Open-source hardware and Arduino output](doc/5arduino/assessment.md)  |   
| 0513 | pre presentation & QA     | 
| 0520 | [Arduino input](doc/5arduino/assessment.md)    | 
| 0522 | [Processing(Interface application programming)](doc/11Interface-application-programming/Assessment.md)|  
| 0527 | [Computer aided design](doc/2cad/Assessment.md)|
| 0529 | QA |
| 0603 |[ Material](doc/12material&tool/assessment.md)| 
| 0605 | Midterm presentation    |  
| 0610 | [3D printing](doc/3_3dprinter/assessment.md)  | 
| 0612 | [Computer-controlled cutting](doc/6laser_cutter/Assessment.md) |    
| 0617 | [IOT ](doc/10IOT/Assessment.md)   |  
| 0619 | QA     |  
| 0624 | QA     |  
| 0626 | Final presentation  |  


# Evolution

Our evolution method is the following

Daily homework full score is 60

| Course Content       | Full Score  |  
| ------------- | :-----:|	
| [Project management](doc/1projectmanage/Assessment1.md)             | 10   | 
| [Arduino output](doc/5arduino/assessment.md)                        | 5    | 
| [Arduino input](doc/5arduino/assessment.md)                         | 5    | 
| [ Interface application programming ](doc/11Interface-application-programming/Assessment.md) |  5   |  
| [Material](doc/12material&tool/assessment.md)                       |  5   |  
| [Computer aided design](doc/2cad/Assessment.md)                     |  10  |  
| [3D printing ](doc/3_3dprinter/assessment.md)                       |  10  | 
| [Computer-controlled cutting](doc/6laser_cutter/Assessment.md)      |  5   | 
| [Arduino IOT](doc/10IOT/Assessment.md)                              |  5   |  


Final presentation full socre is  40



#### [SZPJNF](https://nexmaker-fab.github.io/2024ZWU-IS-1/#/)


| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management      |~~1.add personal webpage（own repository， not in team repository）~~ <br> ~~2. the position  name with logo need check again~~ <br>~~3.how to build webpage~~<br>~~4.need daily homework~~<br>~~5.how to clone,pull,push~~<br>~~6.highlight html~~ <br>~~7.file and folder relationship and show html and css~~|0602|9| 
|  Arduino output   | ~~1.reference link for Automatic plant watering system~~ <br>~~2.need own case not class's case~~<br>~~3.highlight code~~<br>~~4.tinkercad drawing for own case~~<br>~~5.check the size of final video~~ |0602|4| 
|  Arduino input  |~~1.highlight code~~<br>~~2.check the size of video~~ <br>~~3.show the link for sensor~~<br>~~4.show the total physical connect for all components~~|0602|5| 
|  Interface application programming  |~~1.highlight code~~<br>~~2.check the size of video~~ |0602|5| 
| Material  |1.pic<br>~~2. al series~~<br>3.graphene<br>~~4nanpcellulose~~<br>~~5. pic for al anodizing~~<br>~~plastic and wood postprocess~~|0626|4| 
| Computer aided design  |~~1.plugin~~<br>2.motion<br>~~drawing~~|0602 |8| 
|3D printing     |~~1.FDA~~<br>2.pic for SLM material<br>~~3.exchange compoiste~~<br>~~4.postprocessing~~<br>~~5.gcode~~|0626|9| 
| Computer-controlled cutting|1.wavelength<br>~~2.ss series~~<br>3.safety picture|0626|3|  
|  Arduino IOT   | |0626|3| 








#### [Ecobloom](https://nexmaker-fab.github.io/2024ZWU-IS-2-Ecobloom/#/)
| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management      |~~1.add personal webpage（own repository， not in team repository）~~<br>~~2.check"know our teammember"~~<br>~~3.simple introduce your team~~<br>~~4.highligh html coding，not text~~<br>~~5.need high quality picture~~<br>~~6.how to find token for picgo~~<br>~~7.how to use git to clone,pull push repositroy~~<br>~~8.the link for the button project and final work~~| 0602|9| 
|  Arduino output   |~~add your own case for output~~|0602|5| 
|  Arduino input  |  |0602|4| 
|  Interface application programming  | |0602|4| 
| Material  |1.add detail for ss(45,304,316)<br>~~2.postprocess performance~~<br>~~3.pvc~~<br>~~4.pic for wood~~|0626|4| 
| Computer aided design  ||0602|9| 
|3D printing     |~~result for 汉印~~<br>~~3d print matetial (pla,pa12)~~<br>~~4. gcode~~|0624|9| 
| Computer-controlled cutting|~~safety pic~~<br>~~manufature~~|0626|4|  
|  Arduino IOT   | |0626|4| 


#### [Yes or Yes](https://nexmaker-fab.github.io/2024ZWU-IS-3-Yes-or-yes/#/)
| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management      |~~1.show the file and fold relationship~~<br>~~2.show the css file position and all code for css~~<br> ~~3.howto setting the navbar~~<br>~~4. how to build the web in github~~<br>~~5.show how to collaborate ~~<br> ~~6.the the method of build the web,confirm use docsify?~~<br>~~7.show all coding index.html and css~~ |0601| 10| 
|  Arduino output   | ~~1.need licence introduce~~<br>~~2.check the tinkercad line (color and no Wire crossings)~~<br>~~3.add the link for open source project~~<br>4.need own case |0601|4| 
|  Arduino input  | ~~1.check the tinkercad(no Wire crossings)~~<br>~~2.check the code const int switchPin = 2;~~ |0601|5| 
|  Interface application programming  | |0527|5| 
| Material  |~~1.mg detail~~<br>~~2.3-Grinding and polishing~~<br>~~picture for Post-processing heat treatment~~<br>~~3.wood~~|0626|5| 
| Computer aided design  |~~check the sketch ,need constrain all  compoents~~|0602|10| 
|3D printing     | ~~detail for blender~~|0624|10| 
| Computer-controlled cutting|~~1.laser cutter protection~~<br>~~2.environment~~<br>~~3.no glove~~<br>4.~~cnc pic~~<br>~~lasrcutter 890l~~|0624|5|  
|  Arduino IOT   | |0626|4| 

####  [watermelon sugar](https://nexmaker-fab.github.io/2024ZWU-IS-4/#/)
| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management      |~~1.the link is wrong~~<br>~~2.add personal webpage（own repository， not in team repository）~~<br>2.how to use git clone,pull,push<br>3.need detail to express how to buid the web |0601|7 | 
|  Arduino output   |  |0601|3| 
|  Arduino input  |~~1.need own case not the case in class~~<br> ~~2.check the size of picture~~|0601|5| 
|  Interface application programming  |~~1.checking the arduino code(no arduino here)~~<br>~~2.check the video size~~ |0601|5| 
| Material  |~~1.detail for spring（including series~~<br>~~2.injection delete~~<br>~~graphene part~~<br>~~3.postprocess for wood~~|0626|5| 
| Computer aided design  |1.assemble<br>2.motion<br>3. parameter design practice<br>4.Automated Modeling|0602|7| 
|3D printing     |~~1.gcode~~<br>~~2arburg~~|0626|9| 
| Computer-controlled cutting|~~1.lasercutter safey picture~~<br>~~2.laser cutter and material~~ <br>~~cnc  application~~|0624|5|  
|  Arduino IOT   | |0626|4| 

#### [BCZG](https://nexmaker-fab.github.io/2024ZWU-IS-5/#/)
| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management      |~~1.add personal webpage（own repository， not in team repository）~~<br>~~2.how to connect with github~~<br>~~3. how to build repository,collaborate,and web page link~~<br>~~4. show the file and fold relationship~~<br>~~5. show the real file name for"HTML section of homepage code"~~|0601|9 | 
|  Arduino output   |~~1.the board is Raspberry Pi pico~~<br>~~2.code need highlight~~<br>~~3.check the picture size for Raspberry Pi~~ <br>~~4.need own case .not the case from class~~<br> LED connect is wrong |0601|3| 
|  Arduino input  | ~~1.need tinkercad connect~~<br>2.need real condition <br> 3,check the sensor is ultrasonic sensor?|0602|3| 
|  Interface application programming  | |0601|4| 
| Material  |~~1.concrete~~and  composite <br>~~2.ss~~<br>~~3. PE application and update~~<br>~~memory alloy and graphite gif~~<br>~~4. postprocess ss: machine,compare pic~~<br>~~5.PE and wood postporcess~~<br>~~material for final project~~|0626|4| 
| Computer aided design  |1.how to design one model step by step<br>2.test the motion method<br>3.how to assemble|0602|6| 
|3D printing     |~~1.stratasys and EEE~~<br>~~2.nano manufacure~~<br>3.gcode<br>~~4.postporcesing~~ <br>|0626|8| 
| Computer-controlled cutting|~~1.check the picture~~<br>~~2ss series~~<br>~~3.glass~~<br>~~4.leathe disadvantage~~<br>~~5.laser cutter application~~<br>6.cnc application |0626|3|  
|  Arduino IOT   | |0626|4| 

#### [None group](https://nexmaker-fab.github.io/2024ZWU-IS-6/#/)
| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management      |~~1.need personal web link~~<br>~~2.how to set navbar,and other effect~~<br>~~2.the method push to github~~<br>~~3.the method setting the howpage~~<br>~~4.why add gitlab homepage~~<br>~~5. give the reference link for zhihu's correspond information~~<br>~~6.clone 's picture need own picture~~<br>~~7.show the file and fold relationship~~<br>~~8. use docsify or  other method ,~~<br>~~9.how to make the daily homework page~~|0601|10 | 
|  Arduino output   | ~~1.need own case for homework~~<br>~~2. need licence introduce~~ |0602|4| 
|  Arduino input  | ~~1. circuit connect~~ <br> ~~2.U8G2Library'slink~~ <br>~~3.confirm if  the library can run~~|0602|5| 
|  Interface application programming  |~~1.need tinkcad picture~~ |0601|5| 
| Material  |~~1.check the GF~~<br>~~2.detail for SHP~~<br>~~3.detial for ss series~~<br>~~4.annealing:3pictures~~|0626|5| 
| Computer aided design  |~~1.need automated model（need method step by step not result）~~<br>~~2.use plugin to make model(how to use it not just downlaod)~~|0602|10| 
|3D printing     |~~1.gcode analysis~~<br>~~2.check the battle~~<br>~~3.support~~<br>~~4.detial for background~~|0626|10| 
| Computer-controlled cutting|2 methods|0626|4|  
|  Arduino IOT   |1.how to run<br>2.application <br>3.advantage and disadvantage |0625|3| 



******
#### update
* 20240420:build page;
* 20240625:finish






Hi everyone.
The following is our schedule.



| Date        | Course Content           |  
| ------------- | :-----:|	
| 11.10-11.12     | Introduction of design engineering | 
|  11.17-11.19   | Project management      |  
| 11.24-11.26 | Computer aided design    |  
| 12.1-12.3      | Open source hardware and Arduino basic  | 
| 12.8-12.10 |     Arduino (output devices)    | 
|  12.15-12.17    |  Arduino (input devices)    |  
|  12.22-12.24|  Arduino IOT    | 
|   12.29-1.4 | Q&A     |  
| 2.23-2.25 |   Q&A    |  
| 3.2-3.4     | Computer-controlled cutting      |  
|  3.9-3.11    | Midterm presentation ，Q&A     |  
| 3.16-3.18 |  Interface application programming     |  
|  3.23-3.25     |   Introduction of patent application  |  
| 3.30-4.1 |  3D printing /Intelligent materials   |  
|  4.6-4.8 |  Q&A  |  
| 4.13-4.15 | Final presentation   |  




Our students come from  19 countries.
![](https://gitlab.com/picbed/bed/uploads/8e59a0f3777b96ce520aea90359a048e/2021map.png)



#### [The Flash team](https://turatsinzecmu2020.wixsite.com/theflash)

| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Introduction of design engineering | show potential final project idea |20211224|Doing| 
| Project management  | ~~1. show how to build your webpage~~<br>~~2.manage the blog method to show project~~<br>~~3.the github webpage is useless,please check it;~~<br>~~4. the github webpage is different with your team's webpage~~|20220107|Done| 
| Open source hardware and Arduino basic |~~show coding wiht text not picture~~|20211231|Done| 
|  Arduino (output devices) | |20211231|Done| 
|  Arduino (input devices)  | |20211231|Done| 
| Computer aided design |~~1,Cup design :Add every step's screenshot to show how to run the following revolve,shell, fillet,pipe,combine;~~<br>~~2.Cup design:show how to add materail to you design;~~<br>~~3.Project:show how to use the function of "Revolve" and "sweep"~~|20220120|Done| 
|  3D printing  | 1.show the setting in software<br>2.show how to print it <br>3.show how to post process|20220531|pass| 
|~~Intelligent materials~~| ||~~To-do~~|
|~~Arduino IOT~~ | ||~~To-do~~| 
| ~~Midterm presentation ，Q&A~~| ||~~To-do~~| 
| Computer-controlled cutting| checking assessment 4and 5 and show the detail|20220601|HIGH| 
| Interface application programming | ~~1.show coding  in text not picture;~~ <br> ~~2.show gif for result in processing;~~<br> ~~3.show case arduino with processing~~  |20220527|Well| 
| ~~Introduction of design engineering~~ | ||~~To-do~~| 
|~~Video editing~~ | ||| 
| ~~Introduction of patent application~~ | ||~~To-do~~|
| Final presentation | ||Done| 

#### [The Crescent](https://bitenotes.wixsite.com/the-crescent)

| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Introduction of design engineering | show potential final project idea |20211224|Doing| 
| Project management  |1. show how to build your webpage;<br>~~2.check the picture  and text in your personal's introduction~~<br>~~3.delete useless information~~<br>~~4. no link for homework~~ |20220107|Doing|
| Open source hardware and Arduino basic |edit the layout from person's link to team's link which help others' to learn||Doing| 
|  Arduino (output devices) | |20220120|Done| 
|  Arduino (input devices)  | |20220120|Done| 
| Computer aided design ||20220120|Done| 
|  3D printing  | 1.no picture for 3d design<br>2.show the real condition for 3d printing in gif for last 3 picture;<br>3.Show the 3d printing model and  post processing |20220530|Low| 
|~~Intelligent materials~~| ||~~To-do~~|
|~~Arduino IOT~~ | ||~~To-do~~| 
| ~~Midterm presentation ，Q&A~~| ||~~To-do~~| 
| Computer-controlled cutting|1. checking assessment 4and 5<br>2. mention the reference information<br>~~3. give the result table for test example~~|20220601|Middle| 
| Interface application programming | 1.case from web,mention reference information;<br>2.do your own project; <br>3.process connect with arduino|20220530|Middle| 
| ~~Introduction of design engineering~~ | ||~~To-do~~| 
|~~Video editing~~ | ||| 
| ~~Introduction of patent application~~ | ||~~To-do~~|
| Final presentation | ||Done| 

#### [Estimate](https://lushomo19.wixsite.com/estimate)

| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Introduction of design engineering | show potential final project idea |20211224|Doing| 
| Project management  |1.~~show how to build this webpage~~;<br>2.especial how  to  build the button and link,(for example how to design and build  team member's introduction)<br>~~3 member can add personal introduction~~<br>~~4.Delete AVATAR~~<br>~~5.check the project mangement's button ,if no information just delete~~|~~20211224~~ |Doing| 
| Open source hardware and Arduino basic |~~1.show the data of resistance for "LED LIGHT" and "LED screen"~~<br>~~2.show the gif for AUTOMATIC LIGHT SYSTEM~~|20220107|Done| 
|  Arduino (output devices) |~~1.Check the connection of arduino~~<br>~~2.delete class's example or you have new idea~~|20220107|Done| 
|  Arduino (input devices)  | ||Done| 
| Computer aided design |~~1.for "3D BATARANG"show the detail how to make the sketch especial about "mirrror";~~<br> ~~2."3D BATARANG",show the detail of "extrude"(the logo,how to choice in the command board);~~<br>~~3.for "3D BATARANG",show the detail about "draft";~~<br>~~4,for "3d dragonball" show how to revolve in command board;~~<br>5,for 3d dragonball,show how to change appearance<br>6.for "3D Screwdriver",check sketch,how to build model from 2d  to 3d|20220107|Doing| 
|  3D printing  |~~1. show software setting~~<br>~~2.show how to post processing~~ |20220530|High| 
|~~Intelligent materials~~| ||~~To-do~~|
|~~Arduino IOT~~ | ||~~To-do~~| 
| ~~Midterm presentation ，Q&A~~| ||~~To-do~~| 
| Computer-controlled cutting| |20220527|Well| 
| Interface application programming |~~1.show the result for "processing demo with arduino";~~<br>2.mention the reference information and attach link |20220530|High| 
| ~~Introduction of design engineering~~ | ||~~To-do~~| 
|~~Video editing~~ | ||| 
| ~~Introduction of patent application~~ | ||~~To-do~~|
| Final presentation | ||Done| 




#### [Rahmansezad&kltkawsar](https://rahmansezad.wixsite.com/astro)

| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Introduction of design engineering | show potential final project idea |20220107|Doing| 
| Project management  | show to build your webpage|20211224|Doing| 
| Open source hardware and Arduino basic |~~1.Change the color of VCC and GND~~<br>~~2. Optimize the connection~~<br>~~3. try to practice some test about robot here~~<br>~~4.Show the data of resistance~~<br>~~5. add gif to replace jpg;~~<br>~~7.show the coding for arduino Ultrasonic sensor,I find it can't copy the coding~~ |20220118|Done| 
|  Arduino (output devices) | |20211224|Done| 
|  Arduino (input devices)  | |20211231|Done|
| Computer aided design |~~1.delete arduino picture;~~<br>~~2.show how to make sketch ,confirm the size and  position;~~<br>~~3.show how to extrude~~ <br>~~4. design the model in component 1 not ubed~~|20220118 |Done| 
|  3D printing  |show the final model and assemble result |20220527|Middle| 
|~~Intelligent materials~~| ||~~To-do~~|
|~~Arduino IOT~~ | ||~~To-do~~| 
| ~~Midterm presentation ，Q&A~~| ||~~To-do~~| 
| Computer-controlled cutting| 1.mention the reference information and attach link<br> 2.checking assessment 4and 5|20220530|Middle|
| Interface application programming |1.mention the reference information and attach link<br>show example for processing and arduino(radar in vehicle is ok ) |20220530|High| 
| ~~Introduction of design engineering~~ | ||~~To-do~~| 
|~~Video editing~~ | ||| 
| ~~Introduction of patent application~~ | ||~~To-do~~|
| Final presentation | ||Done| 



As a part of Design Engineering 2021,This workshop was launched for ZJU industry design master.
40 students form 8 groups.
This project was launched in Sept 2021
We would list some key points every group need improve before final presentation.
The following is the schedule.








| Date        | Course Content           |  
| ------------- | :-----:|	
| 9.16      | Introduction of design engineering | 
| 9.23      | Project management      |  
| 9.26 | Computer aided design    |  
| 9.30      | Open source hardware and Arduino basic  | 
| 10.14 |     Arduino (output devices)    | 
| 10.21     |  Arduino (input devices)    |  
| 10.28 | Q&A      | 
|11.4     |3D printing    |  
| 11.11 |  Intelligent materials    |  
| 11.18     |Arduino IOT        |  
| 11.25| CNC machine      |   
| 12.2     | Midterm presentation ，Q&A     |  
| 12.9 | Computer-controlled cutting      |  
| 12.16      | Interface application programming  |  
| 12.23 | Video editing      |  
| 12.30    | Introduction of patent application |  
| 1.6 | Q&A     |  
| 1.13 | Final presentation    |  



#### [CNFD](https://374737390.wixsite.com/my-site)

| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Introduction of design engineering | ~~Final project introduce~~||Done| 
| Project management  | ~~1.Update team introduce；~~<br>~~2.Update team member introduce;~~<br>~~3Introduce how to  build web;~~<br>~~4.Layout evry project;~~<br>~~5.delete useless social media link~~|20220113|Done| 
| Computer aided design |~~1.need detail how to design;~~<br>~~2.animation;~~<br> ~~3. how to setting assembly constraints and motion simulation~~|20211107|Done| 
| Open source hardware and Arduino basic |~~1.delete useless information;~~ <br>~~2.check the layout of web;~~ <br> ~~3.Describle the function~~ |20211107|Done| 
|  Arduino (output devices) | |20211107|Done| 
|  Arduino (input devices)  |~~1. Check the coding;~~<br>2.Check the position of word "Arduino input "and "Arduino output" in the link https://374737390.wixsite.com/my-site/toki-top|20220101|Doing| 
|  3D printing  | ~~1.show the detail 3d printing setting in software,expecial the parameter setting;~~<br>~~2. Attach the 3d model data in web;<~~br>~~3.Show how to assemble and connect the circuit~~|202201010|Done| 
| Intelligent materials | ||Done| 
|  Arduino IOT | ||To-do| 
| CNC machine  | ||Done| 
| Midterm presentation ，Q&A | ||Done| 
| Computer-controlled cutting |~~1. Attach the data;<br>Show some key step how to assemble,especial how to assemble with arduino and sensor;<br> **3.Show the parameter setting,how to transport data,run the machine**~~|20220113|Done| 
| Interface application programming |~~1.Test some case communicate processing with arduino~~|20220113|Done| 
| Video editing | ||Done| 
| Introduction of patent application | ||Done| 
| Final presentation | ||Done| 





#### [NBD](https://notblinddesign.wixsite.com/nbdesign)

|Topic|    Question        | Update time           | Condation  |	
| ------------- | :-----:|	:-----:|	:-----:|
| Introduction of design engineering |~~Introduce your potential final project~~|20211107	|	Done|	
| Project management      |   ~~1. Check person link for home page~~<br> 2.The link for project in homepage is wrong<br>~~3.Change the name for project in home page~~ |20211113	|	Doing|	 
| Computer aided design    | ~~1. Introduce how to assemble the model with simple page or gif;~~<br> ~~2.Simple introduce how to do rend  and  animation;~~ <br>~~3.if possible attach  stp or solidworks data;~~<br>~~4.Simple introduce the solidwoks tool~~|20211107	|	Done|	 
| Open source hardware and Arduino basic  |~~1. wrong resistance data;~~<br>~~2.Coding with text not picture~~ |20211107	|Done	|	
| Arduino  (output devices)   | ~~wrong link~~|	20211107|	Done|	
| Arduino (input devices)   | |	20211107|	Done|	 
|  3D printing  |~~1.Show how to operate in software(load model, setting parameter,etc);~~|20220113|Done| 
| Intelligent materials | ||Done| 
|  Arduino IOT | ||Done| 
| CNC machine      |   |	|	Done|	
| Midterm presentation ，Q&A     |  |	|	Done|	
| Computer-controlled cutting      | ~~1.Show the detail parameter about assemble;<br>2.first step open water tank and fan and then running machine;<br>3.not "打印"，use processing or cutting better~~|20220113	|Done|	 
| Interface application programming  |~~1.Show the reason why different button;<br>2."5.Precessing 代码"~~ |20220101	|Done	|	 
| Video editing      |  |	|	Done|	
| Introduction of patent application |  |	|Done	|	
| Final presentation    |  |	|	Done|	

#### [UNDEFINED](https://xxzzxh.github.io/)

|Topic|    Question        | Update time           | Condation  |	
| ------------- | :-----:|	:-----:|	:-----:|
| Introduction of design engineering |~~Final project introduce~~ |	20220113|	Done|	
| Project management      | ~~1.Delete useless social media link;~~<br> ~~2.Check the homepage link(home ,project, team, blog);~~ <br>~~3.Simpl introduce HTML,CSS,BOOTSTRAP  GITHUB relation and how to publish with  GitHub~~|	20211107|	Done|	 
| Computer aided design    | ~~1.How to make the model~~ <br>~~2.The method inside luckin coffee picture;~~<br> ~~3.Method add fusion data to github page~~|		20211014|	Done|	 
| Open source hardware and Arduino basic  |~~1.  Add circuit diagram；~~ <br> ~~2.Check video~~ <br>~~3.Simple introduce the Arduino IDE you used~~|20211020|	Done|	 
| Arduino  (Output devices)   | |20211209|	Done|	
| Arduino (Input devices)   | |	20211209|Done|	 
|  3D printing  | |20220113|Done| 
| Intelligent materials | ||Done| 
|  Arduino IOT | more detail about webpage (how to build and commuincate)|20220113|Doing| 
| CNC machine      |   |	|	Done|	
| Midterm presentation ，Q&A     |  |	|	Done|	
| Computer-controlled cutting      |~~1.Introduce design software;2.Attach dxf data~~ |	20220113|	Done|	 
| Interface application programming  | |20220101	|Done	|	 
| Video editing      |  |	|	Done|	
| Introduction of patent application |  |	|Done	|	
| Final presentation    |  |	|Done|	

#### [2021toast](https://perfect-anger-34c.notion.site/2021-Toast-9d52603d5d39409b8337738a551b5df1)

|Topic|    Question        | Update time           | Condation  |	
| ------------- | :-----:|	:-----:|	:-----:|
| Introduction of design engineering |~~Final project introduce~~ |20220113|	Done|	
| Project management      | <br>~~1. build notion web method~~|	20211107	|Done	|	
| Computer aided design    | ~~1. How to make model （at least bird，gear~~<br>~~2.how to setting the motion~~|	20211014|	Done|	 
| Open source hardware and Arduino basic  | ~~1.Update coding；~~ <br>~~2.Update  gif pic for result；~~<br>3.~~Simple introduce the Arduino IDE you used~~<br>~~4.Write the data for resistance~~|20220113|Done|	
|     Arduino(Output devices)    | ~~1.arduino 2.0 write the data for resistance~~<br>~~2.From Chinese to English~~|20220101	|	Done|	
|   Arduino (Input devices)    | |20220101	|	Done|	 
|  3D printing  | get "prototype" in final project  to the part of daily homework|20220113|Doing| 
| Intelligent materials | ||Done| 
|  Arduino IOT | ||Done| 	
| CNC machine      |   |	|	Done|	
| Midterm presentation ，Q&A     |  |	|	Done|	
| Computer-controlled cutting      |~~add software and machine operate~~  |20220101	|	Done|	 
| Interface application programming  | ~~add a case communicate processing with arduino~~|	20220101|Done	|	 
| Video editing      |  |	|	Done|	
| Introduction of patent application |  |	|Done	|	
| Final presentation    |  |	|	Done|

#### [X-Lab](https://x-labby.webflow.io/)
|Topic|    Question        | Update time           | Condation  |	
| ------------- | :-----:|	:-----:|	:-----:|
| Introduction of design engineering | ~~Final project introduce~~ |20220113|	Done|	 
| Project management      | ~~1.Delete useless information;~~ <br>~~2.How to build website~~|20211014|Done	|	
| Computer aided design    | 1.Add design plugin link; <br>~~2.Introdcue the tool~~ <br>3.show design data if possible|20211020	|Doing	|	
| Open source hardware and Arduino basic  |~~1.Add coding with text not picture;~~<br>~~2.Simple introduce the Arduino IDE you used;~~<br>~~3.Update  gif pic for result；~~<br>4. show the data for resistance;<br>~~5. Delete useless picture(robot)~~|	20220101|Doing	|	
|     Arduino   (Output devices)  | |	20220101|	Done|	
|   Arduino  (Input devices)  ||20220101	|	Done|	
|  3D printing  | ||Done| 
| Intelligent materials | ||Done| 
|  Arduino IOT | ||To-do| 	
| CNC machine      |   |	|	Done|	
| Midterm presentation ，Q&A     |  |	|	Done|	
| Computer-controlled cutting      | |	|	Done|	 
| Interface application programming  | |	|Done	|	 
| Video editing      |  |	|	Done|	
| Introduction of patent application |  |	|Done	|	
| Final presentation    |  |	|	Done|

#### [Get1688](https://1175138076.wixsite.com/get1688)
|Topic|    Question        | Update time           | Condation  |	
| ------------- | :-----:|	:-----:|	:-----:|
| Introduction of design engineering |  ~~Final project introduce~~|	20211107|	Done|	
| Project management      |~~1"our programs"~~ <br>~~2.from part1，part2 to project name;~~<br> ~~3. the link for “张宇琦”;~~ <br>~~4. the layout for designer; ~~  <br>~~5.the link for part1 is useless; ~~<br>~~6.Delete useless information~~ <br>~~7.photoshop~~|20211107	|Done	|	
| Computer aided design    | ~~1.Introudce how to do it one by one;~~<br> ~~2.The layout for video~~<br>3.Check the picture data|20211107	|Doing	|
| Open source hardware and Arduino basic  |~~Arduino IDE how to use it~~|20211107	|Done	|
|     Arduino (Output devices)   | |20211107	|	Done|	
|   Arduino (Input devices)    | |20211107	|	Done|	 
|  3D printing  | ||Done| 
| Intelligent materials | ||Done| 
|  Arduino IOT | ~~1. clear picture for" 2. 阿里云IOT平台设置";<br>2. aliyun setting for clock~~|20220113|Done| 
| CNC machine      |   |	|	Done|	
| Midterm presentation ，Q&A     |  |	|	Done|	
| Computer-controlled cutting      | |	20211209|	Done|	 
| Interface application programming  | |	|Done	|	 
| Video editing      |  |	|	Done|	
| Introduction of patent application |  |	|Done	|	
| Final presentation    |  |	|	Done|


#### [WooHoo](https://richengaa.wixsite.com/woohoo)
|Topic|    Question        | Update time           | Condation  |	
| ------------- | :-----:|	:-----:|	:-----:|
| Introduction of design engineering |~~Final project introduce~~ |	20220113|	Done|	
| Project management      | ~~Simple how to build web~~ |	20211107	|Done	|
| Computer aided design    |~~1. how to design model in creo and fusion 360~~ |	20211107	|Done	|
| Open source hardware and Arduino basic  | ~~1.introduce the tool you have used;~~<br>~~2.Coding for text; ~~<br> ~~3.Arduino IDE simple introduce~~;<br>~~4.Show the result for mpu6050~~|20220101	|Done	|
|     Arduino  (Output devices)   | |	20211107|	Done|	
|   Arduino   (Input devices) | |20211107	|	Done|	 
|  3D printing  |~~1.show the detail 3d printing setting in software,expecial the parameter setting；~~ <br>~~2.show the detail of cross profile which can express why can rotate;~~|20220101|Done| 
| Intelligent materials | ||Done| 
|  Arduino IOT | |20220113|Done| 	
| CNC machine      |   |	|	Done|	
| Midterm presentation ，Q&A     |  |	|	Done|	
| Computer-controlled cutting      | ~~1.Show how to export data from Rhino to dxf~~<br>~~2. Show the detail parameter  which influence the assembly;<br>Show Rhino 3D model and compare with acutal model~~|20220113	|Done|	 
| Interface application programming  | ~~1. Show how to connect arduino with component in Tinker cad or another method;<br>2.Simple introduce "CapacitiveSensor.h"~~|20220101	|Done	|	 
| Video editing      |  |	|	Done|	
| Introduction of patent application |  ||	Done	|	
| Final presentation    |  |	|	Done|


#### [OUTLAST Zone](121.41.45.236)
|Topic|    Question        | Update time           | Condation  |	
| ------------- | :-----:|	:-----:|	:-----:|
| Introduction of design engineering | ~~Final project introduce~~	|20220113|	Done|	
| Project management      | ~~1.team introduce;<br>2.Potential final project;~~<br>3. how to build github web page; <br>~~4.Another page~~|20220113	|Doing	|	 
| Computer aided design    |1.Simple introduce how to make model， assemble and find standard part；<br>2. Attach stp link if possible  |20211014	|Doing	|
| Open source hardware and Arduino basic  | ~~Try to add video to web~~|20211014	|Done	|
|     Arduino  (Output devices)   | |	20220113|	Done|	
|   Arduino (Input devices)   | |	20220113|	Done|	 
|  3D printing  | |20220113|Doing| 
| Intelligent materials | |20220113|Done| 
|  Arduino IOT | |20220113|To-do| 
| CNC machine      |   |	20220113|	Done|	
| Midterm presentation ，Q&A     |  |	20220113|	Done|	
| Computer-controlled cutting      | |	20220113|	Done|	 
| Interface application programming  | |	20220113|Done	|	 
| Video editing      |  |	20220113|	Done|	
| Introduction of patent application |  |	20220113|Done	|	
| Final presentation    |  |	20220113|	Done|







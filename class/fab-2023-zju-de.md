As a part of Design Engineering 2023,This workshop was launched for ZJU industry design master.
Students form 8 groups.
This project was launched in Sept 2023
We would list some key points every group need improve before final presentation.
The following is the schedule.


Daily homework full score is 60

| Course Content       | Full Score           |  
| ------------- | :-----:|	
| Project management  | 10   | 
| Computer aided design  |  10    |  
|   3D printing  |  10  |  
|   Computer-controlled cutting |  10  |  
| Arduino   | 10  |  
|  Interface application programming  |  10   |  



Final presentation full socre is  40 



| Date        | Course Content           |  
| ------------- | :-----:|	
| 0922      | Introduction of design engineering | 
| 0928      | Project management      |  
| 1007 | Intelligent Materials    |  
| 1012     | Computer aided design  | 
| 1019 |   Open-source hardware and Arduino basic  | 
| 1026     |  Arduino (output devices)    | 
|1102 | Arduino (input devices)   | 
|1109   | Midterm presentation |  
| 1116 |   CNC machine   |  
| 1123     |  Interface application programming  |  
| 1130|3D printing     |  
| 1207     | Computer-controlled cutting   |  
| 1214 |  Arduino IOT   |  
| 1221      |Patent application |  
| 1228 |  Q&A     |  
| 0104    | Final presentation  |  


 

#### [InnoWarm](https://nexmaker-fab.github.io/2023zjude-InnoWarm/#/)

| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management      |~~1.detail for build repository and run in local(at least link)~~<br>~~2.how to get Personal Access Tokens~~<br>~~3.how to run docsify~~<br>~~4.detail for fold and file relationship~~<br>~~5.vidoe detail coding~~<br>~~6.recommand Engilsh~~<br>~~7. mention   your final projec idea~~<br>~~8.how to write your first document with markdown at least including tilte,header,picture,link（use one document for example)~~<br>~~9.the link for "https://aooazja.github.io/2023zjude-zja/。"is wrong~~<br>~~10.git use word not picture~~<br>~~11.how to collaborator team~~<br>~~12.markdown add picture and link,try origin method,your method is html method~~<br>~~13.picture size~~ |20231228|10| 
| Computer aided design  |~~1.check picture size~~<br>~~2.constrain~~ ~~the path of rectange~~ ~~and circle in sketch~~<br>~~3.check picture quality~~<br>~~4.show the command detail for "从实体创建零件", "环形阵列" and others,~~<br>~~5. gif for one detail model~~<br> ~~6.add the link of"Fusion 360 APP store"~~<br>~~7.use "GFgear Generator" not "addin sample"~~<br>~~8.how to find zju2023 fold and your team's information~~<br>~~9.Simple parameter design practice~~<br>~~10.test contact set/motion link and show gif;~~<br>11.Engineering Drawing for on component at least<br>~~12.Simple introduce another CAD software or experience~~<br>~~13.information for "软件安装,零件建模,零件建模图例,仿真视频"~~|20240103|9| 
| Arduino  |~~1.need tinkercad for everycase~~<br>~~2.simple introduce arduino IDE layout~~<br>~~3.how to upload coding to Arduino~~<br>~~4.Find some open source projects which are similar with your final project, compare advantage and disadvantage~~<br>~~5.check pic size~~|20240102|9|  
| CNC machine  | ||ok| 
|  Interface application programming  | ~~1.new tool~~<br>~~2.Do one demo in processing and arduino~~ |20240103|8| 
|3D printing     |~~1.key parameter in table~~<br>~~2.problem solution~~<br>~~3.postprocessing method~~<br>~~4.gif for assemble~~<br>~~5.new application~~|20240103|9| 
| Computer-controlled cutting   |~~1.material with picture~~<br>~~2.test~~ <br>~~3. assemble with arduino~~<br>~~4. assemble laser cutter part~~|20240103|9|  
| Final presentation  |  ||soon| 





#### [YSD](https://nexmaker-fab.github.io/2023zjude-YSD/)

| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Introduction of design engineering | |||
| Project management      |~~1.recommand Engilsh~~<br>~~2.mention how to  build team repositry~~<br>~~2.how to run terminal~~<br> ~~3. Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser~~<br>~~4. mention   your final projec idea~~<br >~~5.collaborators change to one admin,other student as maintain~~<br>~~6. how to edit coverpage~~<br>7.detail for fold and file relationship<br>~~8. the method get token~~<br>~~9. the method get pic address with the help of picgo~~<br>10.how to write your first document with markdown at least including tilte,header,picture,link<br>~~11.the relationship "project management" with "fusion 360" in sidebar~~<br>~~12.link useless~~ |20240103|8| 
| Computer aided design  |1.sketch (need constrain)and ~~extrude detail~~<br>2.assemble<br>~~4.3d model insert web~~<br>4.motion method<br>~~5. fusion 360 plugin detail~~<br>6.[practice for Automated Modeling](https://help.autodesk.com/view/fusion360/ENU/?guid=SLD-AUTOMATED-MODELING)|20240104|9| 
| Arduino  |~~1."2.2 Schematic circuit diagram" picture size,~~ ~~coding highlight not picture,~~<br>~~2.how to upload to Arduino~~<br>~~3.need arduino input and output,not the case from class~~<br>4.Find some open source projects which are similar with your final project, compare advantage and disadvantage,IOTproject|20240103|8|  
| CNC machine  | |20240103|ok| 
|  Interface application programming  | |20240103|8| 
|3D printing     |need detail postprocess and assemble ,last time running|20240103|8| 
| Computer-controlled cutting   |~~1.need case assemble with arduino~~<br~~>2.need assemble case from laser cutter~~<br>[3.need result for laser cutter testing](https://nexmaker-fab.github.io/2023zjudem-teamnovva/DailyHomework.html)|20240103|9|  
| Final presentation  |  ||soon| 

#### [NOBRAINNOPAIN](https://nexmaker-fab.github.io/2023zjude-NOBRAINNOPAIN/)

| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management      |~~1."the link for the template of hexo "~~<br>~~2.how to use git  to clone ,pull, push~~<br>~~3.mention   your final projec idea~~<br>~~4.how to write your first document~~<br>~~5.where is local path for "Create a folder for Hexo and create a web project here."~~<br>~~6.how to collaborator team~~<br>~~7.git in terminal use highlight coding not picture~~<br>~~8.how to publish webpage~~<br>~~9.the fold and file relationship~~<br>~~10.how to open git/terminal~~|20231101|10| 
| Computer aided design  |~~1.Design your first 3D model detail~~<br>~~2.Simple parameter design practice;~~<br>~~3.test contact set/motion link and show gif;~~<br>~~4.Try one plugins in fusion 360 app store and used in your design;~~<br>~~5.Engineering Drawing for on component at least；~~<br>~~3d model web live~~|20240102|10| 
| Arduino  |~~1.add own homework~~<br>~~2.Find some open source projects which are similar with your final project, compare advantage and disadvantage~~<br>~~3. show the different pubsubclient.h  with highlight coding~~<br>~~4.how to seting wifi in alicloud~~<br>~~DHT connect to alicloud~~<br>~~the relationship dht11 with "电位器"~~ |20240103|9|  
| CNC machine  | |20240102|ok| 
|  Interface application programming  | |20240103|10| 
|3D printing     |~~3D Printing Advanced Experiment，is it your own case or others case~~|20240102|9| 
| Computer-controlled cutting   |~~assemble all model together and show the gif at least~~|20240103|10|  
| Final presentation  |  ||soon| 





#### [Keyidu](https://nexmaker-fab.github.io/2023zjude-keyidu/#/)

| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management      | ~~1.how to run terminal~~<br>~~2. how to design coverpage(mention index.html and screenshot,and the _coverypage.md location)~~<br>~~3.github repository "NexMaker-Fab/2023zjude-keyidu" at the end of team introduce ,need link~~<br>~~4.add total picture for vscode~~<br>~~5. total information of index.html~~<br>~~6."Problem and solution" need problem picture~~<br>~~7.github clone ,pull,push method~~<br>8. install git during pre deployment<br>9.how to write your first document with markdown at least including tilte,header,picture,link (your information is index.html not document)<br>10.how to collaborator team,need all member's information<br>11.highlight coding  not picture<br>~~12.the fold and file relationship~~<br>~~13.show the total index.html~~|20240103|8| 
| Computer aided design  |~~[1.need new plugin from online app store and used in your design](https://www.autodesk.com/fusionappstore/)~~<br>~~2. the relationship different body or component~~<br>3.detail design for one model<br>~~4.check picture quality~~<br>~~5.video show how to design history for one component~~<br>6.Simple parameter design practice<br>7.Engineering Drawing for on component at least<br>~~8.Simple introduce another CAD software or experience~~|20240103|8| 
| Arduino  |~~no demo video<br>check assessment carefully~~|20240103|9|  
| CNC machine  | ||ok| 
|  Interface application programming  | ~~picture can't read~~|20240103|8| 
|3D printing     |~~1.New 3D printing research~~<br>~~2.how to do post processing~~|20240103|9| 
| Computer-controlled cutting   ||20240103|8|  
| Final presentation  |  ||soon| 


#### [10:36](https://nexmaker-fab.github.io/2023zjude-10-36/)

| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management      |~~1.use github fork to maintain repository,you information should be in "https://github.com/NexMaker-Fab/2023zjude-10-36"~~<br>~~2.recommand English~~<br>~~3. how to run git，open git bush or other software（not github）~~<br>~~4. detail for "Specify the theme"~~<br>~~5.how to write homepage，show the total coding~~<br>~~6.mention about your final project~~<br>~~7.how to use git to git clone,pull,push~~<br>~~8.how to collaborator team，~~need all teammember<br>9.the fold and file relationship(the picture and word to explain)<br>~~10.show all for config.yml~~|20240103|8| 
| Computer aided design  |~~1.how to setting the motion~~<br>~~2.sketch detail need constrain~~<br>~~3.Simple parameter design practice;~~<br>~~4.Try one plugins and used in your design(2pictures diplay problem ,and used in which part of your design );~~<br>~~5.Engineering Drawing for on component at least~~<br>~~6.[practice for Automated Modeling](https://help.autodesk.com/view/fusion360/ENU/?guid=SLD-AUTOMATED-MODELING)~~|20240103|10| 
| Arduino  |~~1.show your own case~~<br>~~2.Find some open source projects which are similar with your final project, compare advantage and disadvantage~~|20240102|9|  
| CNC machine  | ||| 
|  Interface application programming  |~~kinect case by yourself or just copy~~ |20230103|8| 
|3D printing     ||20240102|9| 
| Computer-controlled cutting   |~~1.software setting screenshot~~ <br>~~2.machine practice picture~~|20240103|9|  
| Final presentation  |  ||soon| 





#### [Woodman123](https://design-engineering.littleor.cn/)&[githubpage](https://nexmaker-fab.github.io/2023zjude-Woodman123/)

| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management      | ~~1.check team page"By Team/All Teams/Core"~~<br>~~2.more detail for every step with  screenshot and reference link step4~~<br>~~3.method domain connect with tecent could~~<br>~~4.check git during install node.js with highght coding not picture~~<br>~~5.introduce final project~~<br>~~6.the fold and file relationship~~<br>~~7.how to run git~~<br>~~8.how to add picture~~<br>~~9.how to write the first document~~<br>~~10.where did you purchase domain~~|20240102|10| 
| Computer aided design  |~~1.sketch constrain  and detal at least one~~ <br>~~2.design one model for every step~~<br>3.~~parameter design~~,~~add equation in edit/change parameters~~<br>~~4.[practice for Automated Modeling](https://help.autodesk.com/view/fusion360/ENU/?guid=SLD-AUTOMATED-MODELING)~~|20240103|10| 
| Arduino  |~~1.arduino basic &out need connection drawing,coding,programming method,board introduction~~<br> ~~2.try to make connection drawing for iot &input, need vertical and horizon line~~ <br>~~3.detail for iot setting~~<br>~~4.Find some open source projects which are similar with your final project, compare advantage and disadvantage~~|20240103|10|  
| CNC machine  | |20240103|ok| 
|  Interface application programming  | |20240103|9| 
|3D printing     |~~need one case by one case ,not in same step many case;~~|20240103|9| 
| Computer-controlled cutting   |~~1.laser cutter result~~<br>~~laser cutter result how to connect with arduino~~|20240103|10|  
| Final presentation  |  ||soon| 

#### [FiveTigers](https://nexmaker-fab.github.io/2023zjude-FiveTigers/#/)
| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management      |~~1.how to open terminal~~ <br> ~~2. how to design coverpage(no coverypage) ，check information~~ <br>~~3.the relationship for all fold and file~~<br>~~4. setting coverpage in index.html(all index.html)~~<br>~~5.the relationship git clone github repository and setting docsify~~<br>~~6. use markdown method to put picture in the meantime~~<br>~~git clone,git pull,gitpush~~<br> ~~7.mention about your final project~~ <br>~~8.how to use git to git clone,pull,push~~<br>~~9.how to collaborator team~~<br>~~10.introduce final project~~<br>~~11.the position relationship in for _navbar.md and _sidebar.md~~<br>~~12.show whole information about index.html~~<br>~~13.check if need vue.css and dark.css togther，read index.html carefully~~<br>~~14.from "how to use git" to "how to use github"~~   <br>~~15.add github and githubdesktop link~~<br>~~16.more detail for github practice(picture)~~<br>~~17.the relationship in github for two repositories(build repository first)~~<br>~~18.need your own picture(if reference need mention)~~<br>~~19.need record one step by one step ,not in wrong queue~~<br>~~20.check navbar and sidebar~~|20240104|9| 
| Computer aided design  |~~1.check web model display,The production process,Assembly effect~~<br>~~2.detail process for one model design and Gif to show working conditionconstrain sketch~~<br>~~3 Simple parameter design practice~~ <br>~~4.test contact set/motion link and show gif~~<br>~~5.[practice for Automated Modeling](https://help.autodesk.com/view/fusion360/ENU/?guid=SLD-AUTOMATED-MODELING)~~|20240103|10| 
| Arduino  |~~1.show your own case~~<br>~~2.check picture size~~<br>~~3.Find some open source projects which are similar with your final project, compare advantage and disadvantage~~<br> ~~4.thinkercad drawing,coding,result at least for one case~~<br>~~5. wire need vertical and horizon~~<br>~~6.thinkercad just need one not 2, in Smoke detection lights~~ |20240103|9|  
| CNC machine  | |20240102|ok| 
|  Interface application programming  |~~need your own case~~ |20240103|9| 
|3D printing     |~~need your own case not copy online(mouse)~~<br>~~more detail for software setting and postprocessing~~|20240103|9| 
| Computer-controlled cutting   |~~1.need test result~~|20240102|9|  
| Final presentation  |  ||soon| 

 #### [ORG030](https://nexmaker-fab.github.io/2023zjude-ORG030/)
| Topic       | Question        |  Update time   | Condation | 
| ------------- | :-----:|:-----:|-----:|
| Project management      |~~1. recommand English~~ <br>~~2.check git bash picture~~<br>~~3.where is local fold for your project(check the pic for disk address)~~<br>~~4.screenshot for folder information~~<br>~~5. add key screenshot~~<br>~~6. show how to setting page~~<br>~~7. wrong link for "about" and "group"~~<br>~~8.team & member introdcue~~<br>~~9.the fold and file relationship~~<br>~~10.how to run git~~<br>~~11.how to setting webpage in github~~<br>~~12.the method to show picture~~<br>~~13.the method show how to write one document~~ |20240103|8| 
| Computer aided design  |~~1.detail about sketch~~<br>~~2.high quality picture~~<br>~~3.constrain all detail of sketch~~<br>~~4.check the information in "继续拉伸图中物体"~~<br>~~5. show the result of "阵列"~~<br>~~6.show the information of "选择图形，创建草图，生成投影"~~<br>~~7.fusion 360 app store download and install new plugin~~<br>~~8.Simple parameter design practice;~~<br>~~9.test contact set/motion link and show gif;~~<br>~~10.Engineering Drawing for on component at least~~<br>~~11.Simple introduce another CAD software or experience~~<br>~~12.show 3d living model in web~~<br>~~13.other software introduce~~|20240103|10| 
| Arduino  |~~1.tinkercad for project~~<br>~~2.show your own case~~<br>~~3.Find some open source projects which are similar with your final project, compare advantage and disadvantage~~|20231228|9|  
| CNC machine  | |20240103|ok| 
|  Interface application programming  | where is the document|20231218|7| 
|3D printing     |need postprocessing, assemble and motion for your own model|20240103|8| 
| Computer-controlled cutting   ||20240103|8|  
| Final presentation  |  ||soon| 

******
#### update
* 20230925:build page
* 20231026:check pm&cad

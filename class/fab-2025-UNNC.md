Hi everyone.
The following is our schedule.


| NO| Time        | Course Content           |  
| -------------| ------------- | ------------- |	
|1|0304 |     project manager    | 
|2|0306 |   embedded programming    | 
|3|0311    | electronics design       |  
|4|0318    |electronics production      |  
|5|0325|  Input/Output device    | 
|6|0401    | CAD     |   
|7|0408|    3D scanning and printing   |   
|8|0415 | mid presentation & computer-controlled machining |  
|9|0422 |  molding and casting   |  
|10|0429      | interface and application programming  |  
|11|0506|AI&BP      |  
|12|0513| networking and communications     |  
|13|0520| vehicle case- parameter design        |  
|14|0527|   computer control cutting   | 
|15|0603|  vehicle case-  function test    | 
|16|0610| vehicle debug   | 
|17|0617| vehicle debug |  
|18|0624| final project test |    
|19|0701  | Final project and vehicle case presentation  | 















******
#### update
* 20230419:build this project;
* 20250227:update to v2



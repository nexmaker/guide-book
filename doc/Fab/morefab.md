
## 1. FABLAB -UNNC
* 内容培训
* Fab Lab其他单位合作bob带头
* 宣传内容制作与牵头人

## 2. Software using

#### 2.1 solidworks
* Fab Lab认证机构可以得到一个商业版的license使用权
* Fab Academy的node可以得到1-10个教育版本授权
* fab academy的学生和导师可以使用 SOLIDWORKS standard (opens new window)和 Xdesign (opens new window)云CAD使用；
![](https://raw.githubusercontent.com/bobwu0214/imageuploadservice/main/img/solidworks.png)

## 3.seeed sutdio
* seeed的库、开发版硬件成为 Fab Lab的推荐使用产品
* displaymodule/NexPCB可能有机会成为Fab Lab的供应推荐方之一（Fab Lab的inventory里有vendor&shipping部分），沟通后dm先进入
  
![](https://raw.githubusercontent.com/bobwu0214/imageuploadservice/main/img/WechatIMG79.jpeg)
![](https://raw.githubusercontent.com/bobwu0214/imageuploadservice/main/img/WX20221230-103914.png)

![](https://raw.githubusercontent.com/bobwu0214/imageuploadservice/main/img/WX20230111-214455@2x.png)

## 4.[fab city](https://fab.city/)

![](https://raw.githubusercontent.com/bobwu0214/imageuploadservice/main/img/WX20230125-222713@2x.png)
## 5.[CBA:The Center for Bits and Atoms](http://cba.mit.edu/)
## 6.[FAB23 Bhutan July 16-28, 2023](https://fabevent.org/)

## others
* if we can use github to run project manage,or have to do it in gitlab；
* Could fab academy provide creidts or another method to run generative design/topology design
*  If our lab want to get  Certification by Fab Lab，should we purchase equipment according to [inventory‘s BOM  list](http://inventory.fabcloud.io/),such as Trotec，epilog,GCC? Can we use some Chinese version such as Sunic
* 3D scanner & 3d printer machine ‘s brand is different with inventory



schedule
## Jan 25: principles and practices 
##  Feb 01: computer-aided design
## Feb 08: computer-controlled cutting 
## Feb 15: electronics production 
## Feb 22: 3D scanning and printing
## Mar 01: electronics design 
## Mar 08: computer-controlled machining 
## Mar 15: embedded programming
## Mar 22: molding and casting
## Mar 30: output devices 
## Apr 05: mechanical design, machine design
## Apr 19: input devices 
## Apr 26: networking and communications 
## May 03: interface and application programming 
## May 10: wildcard week 
## May 15 recitation: Fab All-In 
## May 17: applications and implications 
## May 24: invention, intellectual property, and income 
## May 31: project development 
## Jun 07-09-12-14 project presentations
## July 25- FAB17







## information from  Saverio
1. Use nueval website to write down your comments to students' documentation using the feedback function, it will be helpful for you, for the students and for the global evaluator.
2. Push the students to complete at least the first assignment so that you can mark them complete and request a global evaluator early in the semester. You can then talk to the global evaluator and coordinate with him/her.
3. Push your students to make their documentation on time, every wednesday before class they need to have at least something on their page, even if the assignment is not completed: no empty pages.
4. If you have experts and technicians helping with the class, make sure you keep a focus on the "fab lab way" of using machines and concentrate the instruction on what is more useful to complete the assignment.
5. You may have a pre-meeting where you give an overview of what Neil will tell in class on wednesday evening.
6. In your local class, try to show to the student the complete workflow of using a machine in order to complete the assignment, it can be just a very easy example, but with all the steps covered.
7. Try to attend the regional review constantly and push your students to do the same.
8. Neil likes everyone to use the new Attiny microcontrollers that you can program with just one wire (UPDI programmer), mainly the Attiny412 and Attiny1604, for them we don't make the FabISP but we make an FTDI board.
9. If you have questions, the network has the answers 
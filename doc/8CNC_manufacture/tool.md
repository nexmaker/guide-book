We use the following [DIY CNC machine](https://item.taobao.com/item.htm?spm=a230r.1.14.85.736a3286COaS7g&id=605504955634&ns=1&abbucket=18#detail)
![](https://gitlab.com/picbed/bed/uploads/4d4bf48aadf00938f158735b2860338c/Untitled_4.png)
Some key parameter is the following

| Name         | Parameter         | 
| ------------- |-----:|	
| Working voltage| AC220/110V |
| Spindle speed  | 1000rpm     |  	
| working Dimensions| 300 x 200 x 45 mm     |
| Product Dimensions| 417 x 417 x 300 mm |
| Cooling method| No|


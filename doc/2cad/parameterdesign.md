# Parameter design 

## [OpensCAD](http://www.openscad.org/index.html)
OpensCAD use Python to design 3D model.
![](https://gitlab.com/picbed/bed/uploads/1bdbf3eca7634099f54c9c7f728fdef5/logo.png)
![](https://gitlab.com/picbed/bed/uploads/73880d23ca545aa6f9adead4adf8df0b/screenshot.png)



![](https://gitlab.com/picbed/bed/uploads/cdcf1f0b4cf23d4eadc9cb6da967df0d/WX20200730-192533_2x.png)


####   Reference
* [Download software](http://www.openscad.org/downloads.html) 
* [OpenSCAD Tutorial](http://www.openscad.org/documentation.html)
* [Gallery](http://www.openscad.org/gallery.html)


## [Fusion 360 API](http://help.autodesk.com/view/fusion360/ENU/?guid=GUID-A92A4B10-3781-4925-94C6-47DA85A4F65A)
In fusion ,we also can design 3D model or design 3D Plugin with the help of Fusion 360 API


#### Reference 
* [Fusion 360 API](http://help.autodesk.com/view/fusion360/ENU/?guid=GUID-A92A4B10-3781-4925-94C6-47DA85A4F65A)
* [Fusion 360 API github](http://autodeskfusion360.github.io/)

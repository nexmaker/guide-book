

### [1.Periodic Table of the Elements](https://ptable.com/?lang=zh-hans#%E6%80%A7%E8%B4%A8)

![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/perodictableelements.jpeg)



### [2.CMF](https://profabx.com/#/en/prototype/prototype)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/cmf.png)


#### 2.1 Color
There are a few color systems that have been developed over the years to ensure that nothing is lost in translation between designer and manufacturer. 

The most common tools for achieving agreement on color standards are the [Pantone Matching System](https://www.pantone.com/color-systems/pantone-color-systems-explained), and RAL Color System . US-based designers and engineers tend to use the Pantone Matching System, while RAL remains popular in parts of Europe. Manufacturers that work with global customers will typically be familiar with both systems. 


[Pantone Matching System](https://www.pantone.com/color-systems/pantone-color-systems-explained)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/cmyk.png)


[RAL Color System](RAL Color System)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/ral_cassic-CMYK.jpg)

[CMYK](https://www.color-meanings.com/cmyk-color-model/)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/rgbcmyk.png)
Process colors utilize a limited number of inks, such as cyan, magenta, yellow and black (CMYK), applied in different ratios to create a variety of colors. Process colors are generally used when color accuracy or range is less critical.
#### [2.2Material](https://www.pacific-research.com/what-types-of-materials-are-used-in-manufacturing-prl/)
| Material | Common Uses  |Subcategories |  Advantage  | Disadvantage | 
| ------------- | :-----:| :-----:|:-----:|-----:|
| Metals  |This material is typically shiny and lustrous. It is malleable and an excellent electrical conductor|Aluminum<br>Copper<br>Tin<br>Steel<br>Bronze<br>etc|Durable<br>High-quality results<br>Heat and cold resistant |Can be challenging to<br>work with<br>Higher cost<br>Difficult to source | 
|  Polymers |At the molecular level, polymers are comprised of a large number of similar units and can be either synthetic or natural.|Polyethylene<br>Nylon<br>Teflon<br>Cellulose<br>Rubber| Easy to source<br>Inexpensive<br>Simple to work with| Lower-quality results<br>Less durability<br>Limited heat capacity | 
| Ceramics  |These are nonmetallic materials―typically clay, but not always―formed using high heat.|Glass<br>Cement<br>Clay<br>Porcelain<br>Stoneware| Lightweight<br>Robust electrical<br>insulation<br>Inexpensive| Dimensional tolerances can be inconsistent<br>Cracking is possible<br>Processing can be challenging | 
| Composites  |This involves combining two or more disparate materials to create a new, unique material.|Concrete<br>Plywood<br>Fiberglass<br>Paper<br>Reinforced plastics|High durability<br>Customizability<br>Affordability | Can be bad for the environment<br>Repair is challenging<br>May require special handling| 

##### Metal

[Aluminum](https://www.iqsdirectory.com/articles/aluminum-metal/types-of-aluminum.html#/)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/iqsdirectory.png)

| Serial | Main alloying elements  |Principal |  Example application  | 
| ------------- | :-----:| :-----:|:-----:|
| 1xxx |99.xx% pure aluminum| High electrical and thermal conductivity, excellent corrosion resistance.|Electrical conductors and chemical processing equipment.|
|2xxx|Copper|	High strength-to-weight ratio, low corrosion resistance|Truck wheels and suspensions, aircraft fuselage, and wings|
|3xxx	|Manganese	|Moderate strength and good workability.|	General sheet work, recreation vehicles, electronics.|
|4xxx|	Silicon|	Low melting point and thermal expansion, high wear resistance.	|Welding wire and brazing alloy, architectural applications, forged engine pistons.|
|5xxx	|Magnesium	|Moderate-to-high strength, good weldability, good corrosion resistance.|	Appliances, automotive parts, marine components.|
|6xxx|	Silicon and magnesium|	Medium strength with good formability, weldability, machinability, and corrosion resistance.|	Structural applications, architectural extrusions, recreational equipment.|
|7xxx	|Zinc|	Moderate-to-very high strength.	|Airframe structures, mobile equipment, high-stress parts.	|
|8xx.x	|Tin|	Low friction.	|Bearing and bushing applications.|


[Steel](https://www.britannica.com/technology/steel/Wear-resistant-steels)

|Element|	Effect|
| ------------- | :-----:|
|Nickel|	2-5% nickel content increases toughness12-20% nickel content increases corrosion resistance|
|Manganese|	0.25%-0.4% manganese content with a bit of sulphur can lessen brittlenessOver 1% will increase hardenability|
|Silicon|	0.2-0.7% silicon content increases hardenability and strength2% silicon content |increases yield strengthHigh percentages will also give magnetic properties to the alloy steel
|Chromium	|0.5-2% chromium content increases hardenability4-18% chromium content increases corrosion resistance|
|Boron|	0.001-0.003% boron content increases hardenability|
|Copper|	0.1-0.4% copper content increases corrosion resistan|
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/flowline1_540.gif)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/Steel-tubing.png)

[Copper](https://waykenrm.com/blogs/differences-between-brass-bronze-and-copper/#/)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/applications-of-brass-bronze-and-copper-optimized.jpg)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/WX20240602-230239.png)
[Titanium]()
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/WX20240602-231154.png)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/WX20240602-231627.png)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/WX20240602-231627.png)

##### [Polymers](https://www.findlight.net/blog/plastic-manufacturing-processes-brief-guide/#/)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/PlasticsPyramid3.jpg)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/Blog1.2.png)
##### Ceramics
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/WX20240602-234029.png)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/WX20240602-233806.png)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/ceramicsvehicle.png)
##### [Composites](https://www.addcomposites.com/post/where-are-composites-used#/)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/1482836070.materiau.composite.eng.web.jpg)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/fiber-resin-combo.png)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/FiberMatrixComposite3.png)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/WX20240602-234440.png)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/WX20240602-234553.png)


![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/liquidmetal.png)
#### Finish
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/Post-processing-methods-Post-Processing.png)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/bottle3.png)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/WX20240602-235325.png)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/WX20240602-235312.png)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/surface-finish.png)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/1619405031756t.png)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/WX20240603-000623%402x.png)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/WX20240603-000609%402x.png)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/WX20240603-001452.png)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/WX20240603-001907.png)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/WX20240603-001939.png)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/WX20240603-002159.png)
### [3.Performance]()
[Shore hardness test](https://glossar.item24.com/en/glossary-index/article/item//shore-hardness-test.html)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/phpBDSm94.png)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/shore-hardness-scale.jpg)

[Other method](https://www.chegg.com/homework-help/questions-and-answers/compare-influence-different-indenter-shapes-rockwell-hardness-test-please-comment-items-gi-q95179475)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/WX20231219-091144.png)
[Strengh young module](https://slideplayer.com/slide/17420242/)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/youngmodule.png)
[Young-Density module](https://slideplayer.com/slide/17420242/)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/densityyoung.png)
[Strength-Density module](https://slideplayer.com/slide/17420242/)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/strengthdensity.png)
[Max service temparture](https://slideplayer.com/slide/17420242/)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/TEMPERATURE.png)
## Reference
* [Arptech](https://www.arptech.com.au/blog/shore-hardness-scale.htm)
* [Chegg](https://www.chegg.com/homework-help/questions-and-answers/compare-influence-different-indenter-shapes-rockwell-hardness-test-please-comment-items-gi-q95179475)
* [slideplayer](https://slideplayer.com/slide/17420242/)
* [matweb](https://www.matweb.com/search/MaterialGroupSearch.aspx)
*  [Video:CNC](https://www.youtube.com/watch?v=NC63Xx2N1-8&ab_channel=RotherTechnologie)
* [Video:World Exclusive Product Launch From CMF by Nothing](https://www.youtube.com/watch?v=LP7f_fvVJH4&ab_channel=UnboxTherapy)
* [Video:How to get a High gloss finish on a model or prototype](https://www.youtube.com/watch?v=N61PU2ZkYtA&ab_channel=EricStrebel)
* [Apple iPhone manufacturing video](https://www.youtube.com/watch?v=7IA9LWU4iHo&ab_channel=cwvideomaker)
* [Video:Making the all new Mac Pro](https://www.youtube.com/watch?v=GQnr-kfuVdQ&ab_channel=EEStore)
* [Video：How the Apple Watch is made](https://www.youtube.com/watch?v=qmaYUhMH1w8&ab_channel=RandomVids)
* [Video: composite](https://www.youtube.com/watch?v=xGYZqmGjG9s&ab_channel=Ekeeda)

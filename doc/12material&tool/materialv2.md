# Material

## 材料
  * 分类
  * 物理性能:熔点，密度，线胀系数，比热容，导电率，电阻率，
  * 力学性能：硬度，拉伸能力，冲击能力，扭转，压缩，弯曲，剪切，疲劳，
  * 发展趋势
## 金属材料
### 铁/钢
### 铝/合金
### 钛/合金
### 铜/合金
### 钨/合金
### 镍/合金
#### 钛

## 无机非金属材料 
### 陶瓷
### 石英
### 石墨
### 磁性材料

## 高分子材料
### 塑料
### ABS
### PLA
### PP
### 尼龙
### PEEK
### PC
### PEI
### POM
### 人工橡胶
### PU


## 复合材料
### 碳纤维
### 玻璃纤维

## 天然有机材料
### 木
### 天然橡胶
## 其他
### 超塑性合金
### 多孔金属
### 记忆合金
### 薄膜材料
### 储氢合金



abs.md
pla.md
pa.md
pacf.md
pagf.md
pp.md
resin.md
tpu.md
cf.md
pom.md
al.md
ss.md
cu.md
ti.md
metal.md
Polymers.md
Ceramics.md
Composites


abs-en.md

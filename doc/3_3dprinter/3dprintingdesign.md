   ## Schedule
1. Basic Learn
   * Machine introduce and software learn
   * checking the basic performance with test model 
   * Make daily life model
  
2. Assemble and parameter define
    * Setting machine parameter
    * Use [Meshmixer](https://meshmixer.com/) to edit model
    * Make the first assemble model
    * Make mouse or light in daily use
3. Design guide 
    * [Stable diffusion support 2D picture](https://wiki.bambulab.com/zh/knowledge-sharing/CMYK-color-lithophane-printing-instructions)
    * Engineering 3d model manufacture 
    * Make lightweight model
4. Better quality and industrt 3D printing machine
    * Introduce the better quality method
    * Introduce the industry 3D printing manchine
    * Show this class result 




## Reference

#### test model
<img src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/testship.png"  align = "center"  width="1000" />

[ship test(parameter and performance)](https://makerworld.com/zh/models/85074#profileId-90962)

<img src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/flexible.gif"  align = "center"  width="1000" />

[test flexible model](https://makerworld.com/zh/models/104901#profileId-111938)


<img src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/cuble.png"  align = "center"  width="1000" />

[cube test assemble](https://makerworld.com/zh/models/99218#profileId-105803)

<img src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/testtolerance.png"  align = "center"  width="1000" />

[test tolerance](https://makerworld.com/zh/models/86953#profileId-93255)

<img src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/hole.png"  align = "center"  width="1000" />

[hole test](https://makerworld.com/zh/models/109927)

<img src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/angle.png"  align = "center"  width="1000" />

[test angle](https://www.thingiverse.com/thing:40382)

<img src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/bridge.png"  align = "center"  width="1000" />

[bridge test](https://www.thingiverse.com/thing:546688)

#### 2D model
<img src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/3D-1.png"  align = "center"  width="1000" />

 Chinese tradition culuture

<img src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/2DCARTOON.png"  align = "center"  width="1000" />

 cartoon

 <img src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/sd.png"  align = "center"  width="1000" />

 [stable diffusion make 2d picture](https://wiki.bambulab.com/zh/knowledge-sharing/CMYK-color-lithophane-printing-instructions)
#### 3D model  in daily life
<img src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/WX20240101-193955.png"  align = "center"  width="1000" />
<br>[fandesign model](https://www.thingiverse.com/thing:6304044)

<img src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/assemble1.png"  align = "center"  width="1000" />
<br>[3D Printable Fabric](https://www.thingiverse.com/thing:3096598)

<img src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/WX20240101-192853.png"  align = "center"  width="1000" />
<br>[mourse](https://makerworld.com/zh/models/13716#profileId-14573)

<img src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/penholder.png"  align = "center"  width="1000" />
<br>[penholder](https://makerworld.com/zh/models/97250#profileId-103771)

<img src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/10651701921772_.pic.jpg"  align = "center"  width="1000" />

<br>airtag part

<img src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/newyark.png"  align = "center"  width="1000" />
<br>[New York City ](https://makerworld.com/zh/models/106264#profileId-113427)

#### 3D model in lightweight


#### 3D model in engineering
<img src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/gearbox.png"  align = "center"  width="1000" />
<br>[GEARBOX](https://www.thingiverse.com/thing:3726483)

<img src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/windupcar.png"  align = "center"  width="1000" />

[Windup motor Car toy](https://www.thingiverse.com/thing:1848663)

<img src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/gearrotio.png"  align = "center"  width="1000" />

[gear ratio](https://www.thingiverse.com/thing:4683656)

<img src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/ramhand.png"  align = "center"  width="1000" />

[Ram Hand](https://www.thingiverse.com/thing:2605203)

<img src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/enigineer.png"  align = "center"  width="1000" />

[Jet Engine](https://makerworld.com/zh/models/96461#profileId-102873)

<img src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/excavator.png"  align = "center"  width="1000" />

[excavator](https://makerworld.com/zh/models/88796#profileId-95109)

<img src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/eiffel.png"  align = "center"  width="1000" />

[eiffeltowel](https://www.thingiverse.com/thing:2717451)

#### future
<img src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/WX20240101-193601.png"  align = "center"  width="1000" />

[drone](https://www.thingiverse.com/thing:2028741)


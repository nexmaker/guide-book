## Calculation
*  U=I*R
*  P=U*I
*  X<sub>L</sub>=2 $\pi$ fL       (just in AC)
*  X<sub>C</sub>=1/(2 $\pi$ fC)   (just in AC)


## AC and DC

![](https://gitlab.com/picbed/bed/uploads/421422bbce4b80bfc3b0d91f67e3dbb7/AC-vs-DC-1.jpg)
![](https://gitlab.com/picbed/bed/uploads/76027f04b24f8c24a605b1d83e67cb72/acdc.png)
![](https://gitlab.com/picbed/bed/uploads/4da9144e6e9a430a4097a3a311a7f946/WX20200908-102801_2x.png)

## Component

![](https://gitlab.com/picbed/bed/uploads/e48485a5f5aff592d4702750b250b6f0/shutterstock_1238283463.jpg)



## Reference
* [Tempoautomation](https://www.tempoautomation.com/blog/how-the-electronics-component-list-can-make-or-break-pcb-development/)
* [Electronic components](http://www.caraudiohelp.com/electronic_components.html)
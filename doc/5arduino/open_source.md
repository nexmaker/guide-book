
##  1. Define
Open source products include permission to use the source code, design documents, or content of the product. It most commonly refers to the open-source model, in which open-source software or other products are released under an open-source license as part of the open-source-software movement. Use of the term originated with software, but has expanded beyond the software sector to cover other open content and forms of open collaboration.
[Wiki](https://en.wikipedia.org/wiki/Open_source)

![](https://gitlab.com/picbed/bed/uploads/725d67b3da36f07f5bc25bf9a3e0db4e/OpenSourceOverTime.png
)
[reference from hpcwire](https://www.hpcwire.com/2017/06/15/opensuco-open-source-supercomputing-isc/)




## [2. Difference licence](https://opensource.org/licenses/)

![](https://gitlab.com/picbed/bed/uploads/d617f6a9c86081a24e26d4290e69d31a/PERMISSIVE-VS-COPYLEFT-LICENSES-2.jpg)


## 3.Programming language

![](https://raw.githubusercontent.com/bobwu0214/imageuploadservice/main/img/language023.png)
[From IEEE2023](https://spectrum.ieee.org/the-top-programming-languages-2023)



## 4. Reference
* Software
  * [linux](https://www.linux.org/)
  * [Android](https://www.android.com/)
  * [Wordpress](WordPress.org)
* Hardware
  * [Arduino](www.arduino.cc)
  * [Raspberrypi](https://www.raspberrypi.org/)
  * [Opendesk](https://www.opendesk.cc/)
  * [Openrov](https://www.sofarocean.com/)
  * [Localmotor](https://localmotors.com/)
  * [OSVehicle](http://osvehicle.com/)
  * [Reprap](https://reprap.org/wiki/RepRap)
* Other information
  * [Instructables](https://www.instructables.com/)
  * [Arduino 中文社区](https://www.arduino.cn)
  * [Linux china](https://linux.cn/)
  * [Open source in wiki](https://en.wikipedia.org/wiki/Open_source)
  * [Baidu](https://baike.baidu.com/item/%E5%BC%80%E6%94%BE%E6%BA%90%E4%BB%A3%E7%A0%81/114160?fromtitle=Open%20Source&fromid=2667585&fr=aladdin)

# Access and Safety

For safety, we  divide different members who own different right: 
**Visitor** ,**Graduate** ,**Supervisor**
If you want to to do project, you can apply a tool box for your project,and we attach the link:[Tool box application](https://docs.google.com/spreadsheets/d/16YrZSg6tmvkSPjY2PUaWezGl4p_KxTKG7eFvPwa2ddA/edit?usp=sharing).

For every in the lab, Before you leave the lab, we should make sure the lab is cool for everybody:

* If your project not finish and need to do again after today,
    * You can put your project in your tool box, put tool to lab cabinet(remind:not in your tool box);
    * If project is too big and can't put inside the tool box, you need to discuss with supervisor about storage location;
* If you finish your project ,you should clean your project in the lab and return the tool box,then update [Tool box application form](https://docs.google.com/spreadsheets/d/16YrZSg6tmvkSPjY2PUaWezGl4p_KxTKG7eFvPwa2ddA/edit?usp=sharing)

<font color=#ff0000 size=14 face="黑体">Remind:don't arbitrary put your project on your working space and it would influence lab's manage. Supervisor would deal with them. </font>



## For vistor
1. Definition:The member who is NexPCB's staff  but not graduate from NexMaker academy. In addition it include customer, supplier, NexPCB's friend; 
2. Right: 
    * You can apply to attend NexMaker academy and to be a graduate;
    * If you want go to lab for  visit  or doing some working(design, manufacture, test), you  need apply with supervisor through email(detail in "For supervisor" part);
    * Visitor has no right to use 3D printer, laser cutter, CNC ,drilling machine and grinder.
    * <font color=#ff0000 size=14 face="黑体">If you have any potential dangerous or dangerous, you can use first aid kits(we would add soon) and inform supervisor as soon as possible</font>


3. Duty:keep safety
4. Open time: just in NexPCB's working time,and you need get supervise from supervisor;

## For graduate
1. Definition: The member who is graduate from NexMaker academy.
2. Right:
    * You can get access for door's key;
    * You can use all the machine without supervisor during working time;
    * You can apply for material and equipment [(Reference "material")](material.md)
    * <font color=#ff0000 size=14 face="黑体">If you have any potential dangerous or dangerous, you can use first aid kits(we would add soon) and inform supervisor as soon as possible</font>
3. Duty:
    * You need send application email to supervisor and get approve if you want to use lab after working time;
    * You need get supervisor's surprise if you want to use 3D printer, laser cutter, CNC ,drilling machine and grinder;
    * Clean your workspace after working, and throw rubbish to bin;
    * If your project need to do again, you need 
        * Put your project in specified tool box, if tool box can't hold your project, you can discuss with Bob;
        * Put tool to origin position;
4. Open time: 
    * Normal in NexPCB's working time;
    * If you want to work not in working time, please use email to supervisor and get approve;



##For supervisor

1. Definition: The member who is the administrator of lab, now two member [Bob:bob@nexpcb.com](bob@nexpcb.com) and [Jacen:jacen@nexpcb.com](jacen@nexpcb.com)
2. Right: 
    *   Supervisor can use any machine in the lab;
    *   Supervisor have right to change the access of member(add or delete)
    *   <font color=#ff0000 size=14 face="黑体">If you have any potential dangerous or dangerous, you can use first aid kits(we would add soon) and inform another supervisor as soon as possible</font>

3. Duty 
    * Keep the safety of lab;
    * Keep the clean of lab;
    * Confirm the condition of door and windows;
    * <font color=#ff0000 size=14 face="黑体">You need support dangerous condition as soon as possbile;</font>
4. Open time: You can go to lab anytime you need;

*****

#### Update time:
* 20200305

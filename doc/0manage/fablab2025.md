## Fabacademy 2025
FabLab academy trainning would run from Jan 8-Aug 11,every week would run online class 6hours,offline class 6 hours.The following is the schedule,
* Jan 8-12: instructor boot camp
* Jan 15-19: student boot camp
* Jan 24: principles and practices, presentations, introductions (video), project management (video, review)
* Jan 29 recitation: version control (video)
* Jan 31: computer-aided design (video, review)
* Feb 07: computer-controlled cutting (video, review)
* Feb 12 recitation: AI (video)
* Feb 14: electronics production (video, review)
* Feb 21: 3D scanning and printing (video, review)
* Feb 26 recitation: programming and debugging (video)
* Feb 28: embedded programming (video, review)
* Mar 06: computer-controlled machining (video, review)
* Mar 11 recitation: sustainable materials (video)
* Mar 13: electronics design (video, review)
* Mar 20: output devices (video, review)
* Mar 25 recitation: machine building (video)
* Mar 27: mechanical design, machine design (video, review)
* Apr 03: break, midterm review
* Apr 10: input devices (video, review)
* Apr 15 recitation: fab ecosystem (video)
* Apr 17: molding and casting (video, review)
* Apr 24: networking and communications (video, review)
* Apr 29 recitation: education (video)
* May 01: interface and application programming (video, review)
* May 08: wildcard week (video, review)
* May 13 recitation: Fab All-In (video)
* May 15: system integration (video, review)
* May 22: applications and implications, project development (video, review)
* May 27 recitation: start-ups (video)
* May 29: invention, intellectual property, and income (video)
* Jun 05-: project presentations (5, 7, 10, 12)
* Aug 04-11: FAB24
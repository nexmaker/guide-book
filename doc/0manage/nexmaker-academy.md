# NexMaker Academy

Since 2020 ,we cooperate with NexPCB,university with following workshop



* [1.Project manage](https://git-scm.com/)
* [2. CAD design](doc/2cad/cad.md)
* [3. 3D printer](doc/3_3dprinter/assignment.md)
* [4. Electric design ](doc/4electric_design/basicknowledge.md)
* [5. Arduino application](https://www.arduino.cc/)
* [6. Laser cutting](doc/6laser_cutter/basic.md)
* [7. PCB manufacture](https://www.nexpcb.com/blog/smt-pcb-puzzle)	
* [8. CNC manufacture](https://astromachineworks.com/what-is-cnc-machining/)
* [9. IOT and Interaction](https://en.wikipedia.org/wiki/Internet_of_Things)
* [10. Interface Application Programming](http://fabacademy.org/2018/docs/FabAcademy-Tutorials/week16_interface_and_application_programming/processing_resources.html)
* [11.Material and tool](doc/11Standard_component/standardcomponentpart.md)
* [12.Business plan basic](doc/12BP/README.md)

Since 2023,we launch Fablab Ningbo- NexMaker node local and provide [Fab academy](http://fabacademy.org/) workshop for students


## Fab academy Final project


[3D printing SN nozzle Bob fab academy2018](https://fabacademy.org/2018/labs/fablaboshanghai/students/bob-wu/)
<video src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/video/finalvideov2.mov" controls="controls" width="100%" ></video>


[Bio-signal data collection tool kit using for motion sicknessdetection Yaorun fab academy2023](https://fabacademy.org/2023/labs/ningbo/students/yaorun-zhang/)

<video src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/video/2023yaolun.mp4" controls="controls" width="100%" ></video>








## 2020 Design engineering final project


[0924:The piano of light](https://www.nexmaker.com/class/zju2020/924.html)
<video src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/video/zjude2020-0924.mp4" controls="controls" width="100%" ></video>

[Duck Dog:Plant robot](https://1909097669.wixsite.com/duck-dog)
<video src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/video/zju2020duckdog.mp4" controls="controls" width="100%" ></video>

[D&L:Fight with COVID-19](class/zju2020/dlight.md)
<video src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/video/zju2020-dlight.mp4" controls="controls" width="100%" ></video>

[happly planet ](class/zju2020/happlyplanet.md)
<video src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/video/zju2020-happyplant.mp4" controls="controls" width="100%" ></video>

[Coffe Bit](https://manatee257.github.io)
<video src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/video/zju2020-CoffeBit.mp4" controls="controls" width="100%" ></video>


[33Designer](https://www.nexmaker.com/class/zju2020/33design.html)
<video src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/video/zju2020calculation.mov" controls="controls" width="100%" ></video>

[YYclub](class/zju2020/yyclub.md)
<video src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/video/zju2020yyclub.mp4" controls="controls" width="100%" ></video>

[Morning](class/zju2020/morning.md)
<video src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/video/zju2020morning.mp4" controls="controls" width="100%" ></video>

[Natural](class/zju2020/natural.md) 
<video src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/video/zju202natural.mp4" controls="controls" width="100%" ></video>

## 2021 Design engineering/manufacuture/mini final project
[CNF](https://374737390.wixsite.com/my-site/%E5%89%AF%E6%9C%AC-arduino-1)
<video src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/video/zju2021decnf.mp4" controls="controls" width="100%" ></video>

[NB-design](https://notblinddesign.wixsite.com/nbdesign/%E5%89%AF%E6%9C%AC-final-project)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/ZJUDE2021.png)

[UNDEFINED](https://xxzzxh.github.io/final.html)
<video src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/video/zju2021deundefined.mp4" controls="controls" width="100%" ></video>

[Get1688](https://1175138076.wixsite.com/get1688)
<video src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/video/zju2021deget1688.mp4" controls="controls" width="100%" ></video>

[Woohoo](https://richengaa.wixsite.com/woohoo/%E5%89%AF%E6%9C%AC-project-1-1)
<video src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/video/zju2021dewoohoo.mp4" controls="controls" width="100%" ></video>

[The flash team](https://turatsinzecmu2020.wixsite.com/theflash/projects-6-1)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/zju2021dm.png)

[The crescent](https://bitenotes.wixsite.com/thecrescent/design-and-fabrication-of-automatic-baby)
<video src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/video/zju2021dmCrescent.mp4" controls="controls" width="100%" ></video>

[Rahmansezad](https://rahmansezad.wixsite.com/astro)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/WX20231228-233817.png)

## 2022 Design engineering/manufacuture/mini final project

## 2023 Design engineering/manufacuture/mini final project
[Fivevolution](https://nexmaker-fab.github.io/2023zjudemini-hi1/#/md/06_Final_presentation)
<video src="https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/video/zju2023deminiFIVEREVOLUTION.mp4" controls="controls" width="100%" ></video>

[Fivist](https://nexmaker-fab.github.io/2023zjudemini-hi2/)
![](https://nexmaker-profabx.oss-cn-hangzhou.aliyuncs.com/img/2023DEMINIFivist.gif)

*****

#### Update time:
* 20200304:v1
* 20230110:v2 delete nexmaker  acadmey schedule and add Fablab ningbo- NexMaker
* 20231130:v3 add final project2020
* 20231228:v4 add final project 2021-2022

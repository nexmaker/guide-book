## 1. Basic knowledge about laser cutter

![](https://gitlab.com/picbed/bed/uploads/544c5974ab0d136f2ea1d8b958295072/laser-cut.gif
)

Laser cutting is a technology that uses a laser to cut or engraving materials, and is typically used for industrial manufacturing applications, but is also starting to be used by schools, small businesses, and hobbyists. Laser cutting works by directing the output of a high-power laser most commonly through optics. The laser optics and CNC (computer numerical control) are used to direct the material or the laser beam generated. A typical commercial laser for cutting materials involved a motion control system to follow a CNC or G-code of the pattern to be cut onto the material. The focused laser beam is directed at the material, which then either melts, burns, vaporizes away, or is blown away by a jet of gas, leaving an edge with a high-quality surface finish. Industrial laser cutters are used to cut flat-sheet material as well as structural and piping materials.


* In 1965, the first production laser cutting machine was used to drill holes in diamond dies. This machine was made by the Western Electric Engineering Research Center; 
* In 1967, the British pioneered laser-assisted oxygen jet cutting for metals;
* In the early 1970s, this technology was put into production to cut titanium for aerospace applications. At the same time CO2 lasers were adapted to cut non-metals, such as textiles, because, at the time, CO2 lasers were not powerful enough to overcome the thermal conductivity of metals.
  
   reference from [Wiki](https://en.wikipedia.org/wiki/Laser_cutting)
***



* The basic structure of laser cutter

![](https://gitlab.com/picbed/bed/uploads/56743b8fe5e398e3c1a3562977543f97/cnc-laser-system.png)
Reference from [advanced machinery](https://am.co.za/laser/cabinet)



* The nozzle of laser cutter
![](https://gitlab.com/picbed/bed/uploads/9f62d3f7d32249b67301603249d1d919/LaserCutter.jpg
)



## 2. Computer-Controlled Cutting
* [Knife](https://www.youtube.com/watch?v=PG9lJOnNTzQ):Use knife to cut the profile of material according to computer control machine. The following is some vendors [Vinyl Cutter Machines](https://www.rolanddga.com/products/vinyl-cutters) ,[Zund](https://www.zund.com/en) ,[Othercutter](https://www.youtube.com/watch?v=PG9lJOnNTzQ)
  ![](https://gitlab.com/picbed/bed/uploads/b632e9d47d801a0e7dc233cb9afe85e3/VINLY.png)
 
 
* Laser:we can divide  CO2 laser,Fiber lasers and The neodymium (Nd) and neodymium yttrium-aluminium-garnet (Nd:YAG) lasers ,introduce in the below PART3. The following is some vendors:[Epilog](https://www.epiloglaser.com/laser-machines/product-line.htm) ,[Universal](https://www.ulsinc.com/technology/products),[Trotec](https://www.troteclaser.com/) 
     
* [Plasma:A process that cuts through electrically conductive materials by means of an accelerated jet of hot plasma.](https://en.wikipedia.org/wiki/Plasma_cutting). The following is some vendors,[Forest Scientific](http://forestscientific.com/cnc-plasma-cutters/) ,[Torchmate](https://torchmate.com/how-to-choose)
![](https://gitlab.com/picbed/bed/uploads/a0a3eaaac5305b1d6e4d096a24ba3fe7/PlasmaCutteractionpix.jpg)

* [waterjet:An industrial tool capable of cutting a wide variety of materials using a very high-pressure jet of water, or a mixture of water and an abrasive substance](https://en.wikipedia.org/wiki/Water_jet_cutter)
![](https://gitlab.com/picbed/bed/uploads/22d4518258963a56bf19a5c66e4dbcc0/abrasive-waterjet.gif)

* Hot wire,Used to cut foam. The following is some vendors:[FROGWire](http://www.3dcutting.com/solutions/frogwire.html) ,[Hotwire](https://hotwiresystems.com/hot-wire-cnc-foam-cutters/), [MTM](http://ng.cba.mit.edu/show/video/14.08.modular.mp4)
* [Electrical discharge machining (EDM), also known as spark machining, spark eroding, die sinking, wire burning or wire erosion, is a metal fabrication process whereby a desired shape is obtained by using electrical discharges (sparks)](https://en.wikipedia.org/wiki/Electrical_discharge_machining). The following is some vendors:[Sodick MTM](https://www.sodick.com/),[Mitsubishielectric](https://www.mitsubishielectric.co.jp/fa/products/mecha/edm/premium/index.html)

![](https://gitlab.com/picbed/bed/uploads/3abfee1919052b48973291dffe78a9c2/edm1.jpg)
![](https://gitlab.com/picbed/bed/uploads/4a943064c01aab8550101aa55d06dfa1/edm2.jpg)
![](https://gitlab.com/picbed/bed/uploads/2547df2f87a855d0f7f661260c1344f2/Electrical_discharge_Machining.gif)
## 3. Laser cutter type

There are three main types of lasers used in laser cutting. 
* The CO2 laser is suited for cutting, boring, and engraving. CO2 lasers are commonly "pumped" by passing a current through the gas mix (DC-excited) or using radio frequency energy (RF-excited). The RF method is newer and has become more popular. Since DC designs require electrodes inside the cavity, they can encounter electrode erosion and plating of electrode material on glassware and optics. Since RF resonators have external electrodes they are not prone to those problems. CO2 lasers are used for industrial cutting of many materials including titanium, stainless steel, mild steel, aluminium, plastic, wood, engineered wood, wax, fabrics, and paper. 
* Fiber lasers are a type of solid state laser that is rapidly growing within the metal cutting industry. Unlike CO2, Fiber technology utilizes a solid gain medium, as opposed to a gas or liquid. The “seed laser” produces the laser beam and is then amplified within a glass fiber. With a wavelength of only 1.064 micrometers fiber lasers produce an extremely small spot size (up to 100 times smaller compared to the CO2),making it ideal for cutting reflective metal material. This is one of the main advantages of Fiber compared to CO2.


* The neodymium (Nd) and neodymium yttrium-aluminium-garnet (Nd:YAG) lasers are identical in style and differ only in application.
  *  Nd is used for boring and where high energy but low repetition are required. 
  *  The Nd:YAG laser is used where very high power is needed and for boring and engraving. 


#### 4.Material

* Acrylic
* Glass
* Metal
* Textiles
* Wood






## 5.The laser cutter in our lab

![](https://gitlab.com/picbed/bed/uploads/1940a8cbe0f567c4b2e3adc15fee716a/WechatIMG80.jpeg
)


|Product|HX960      | 
| ------------- |-----:|	
|Working area|900*600mm|
|Machine size |1500*1200*1100mm    | 
| Engraving speed|0-600mm/s|   	
| Laser power|80W  |
|Resolution ratio|0.025mm|
|The min word|Chinese Character2mm /letter1mm|
|Power|AC220V±10%,50HZ,<2500W|
|Formate|AI, BMP、JPEG、PCX、TGA、CDR、DWG、TIFF、PLT、DXF|
	


## 6. Reference
* [Recording from fablab 2018](http://fab.academany.org/2018/lectures/fab-20180207.html)
* [Basic infromation about laser cutter](http://academy.cba.mit.edu/classes/computer_cutting/index.html)

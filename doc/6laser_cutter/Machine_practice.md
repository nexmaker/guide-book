## 1. Powercut(laser cutter software) install

![](https://gitlab.com/picbed/bed/uploads/967a90f13442f1ee37ed3c96044aff8e/powercut.png)

## 2.  Prepare machine

#### 2.1 Check connection
* Check power for laser cutter, power for smoke exhaust fan open and running
  
![](https://gitlab.com/picbed/bed/uploads/7c83e2d3254ba5262d3c7ba00721dcee/part.png)

* Open the laser cutter power

<div align=center>
	<img src="https://gitlab.com/picbed/bed/uploads/6d2b0b375df7297172fbb867a584484b/WechatIMG84.jpeg" width="1000"> 
</div>

#### 2.2 Load material 

Put material flat to platform and then close the door
![](https://gitlab.com/picbed/bed/uploads/9588cdde5ac47cc7a32ae2932e5160bb/WechatIMG86.jpeg)


#### 2.3 Focus point
In focus distance the process would be good.
  
![](https://gitlab.com/picbed/bed/uploads/3cc5e2b0029bde30e41230b35a7aceb8/focus_point.png)


## 3. Machine running

![](https://gitlab.com/picbed/bed/uploads/d8ef4b622e52293dd634a354739c50a5/machine_running.png)